package com.tcl.onetouch.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tcl.onetouch.R;

public class ImageDisplay {
	protected static ImageLoader imageLoader = ImageLoader.getInstance();
	protected static DisplayImageOptions options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.logo_3d).showImageForEmptyUri(R.drawable.logo_3d)
			.showImageOnFail(R.drawable.logo_3d).cacheInMemory(true).cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(false).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.resetViewBeforeLoading(false).delayBeforeLoading(200).build();

	protected static ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	protected static ImageLoadingListener contentDetailImageDisplayListener = new ContentDetailImageDisplayListener();
	protected static ImageLoadingListener userIconImageDisplayListener = new UserIconDisplayListener();
	protected static ImageLoadingListener myTagImageDisplayListener = new MyTagDisplayListener();
	protected static ImageLoadingListener circleImageDisplayListener = new CircleImageListener();
	protected static ImageLoadingListener commentsUserDisplayListener = new CommentsUserImageListener();

	// static final List<String> displayedImages =
	// Collections.synchronizedList(new LinkedList<String>());

	private static void addImage(String imageUri) {
		// if (!displayedImages.contains(imageUri)) {
		// displayedImages.add(imageUri);
		// }
	}

	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//			if (loadedImage != null) {
//				// ImageView imageView = (ImageView) view;
//				// boolean firstDisplay = !displayedImages.contains(imageUri);
//				// if (firstDisplay) {
//				// FadeInBitmapDisplayer.animate(imageView, 1000);
//				addImage(imageUri);
//
//				// }
//
//				int imageHeight = loadedImage.getHeight();
//				int imageWidth = loadedImage.getWidth();
//
//				try {
//					LayoutParams layout = view.getLayoutParams();
//					layout.height = view.getWidth() * imageHeight / imageWidth;
//					layout.width = view.getWidth();
//					if (layout.height == 0 && layout.width == 0) {
//						layout.height = imageHeight;
//						layout.width = imageWidth;
//					}
//					view.setLayoutParams(layout);
//				} catch (Exception e) {
//				}
//			}
		}
	}

	private static class ContentDetailImageDisplayListener extends SimpleImageLoadingListener {

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				// boolean firstDisplay = !displayedImages.contains(imageUri);
				// if (firstDisplay) {
				// FadeInBitmapDisplayer.animate(imageView, 1000);
				addImage(imageUri);
				// }

				int imageHeight = loadedImage.getHeight();
				int imageWidth = loadedImage.getWidth();

				try {
					LayoutParams layout = view.getLayoutParams();
					layout.width = view.getHeight() * imageWidth / imageHeight;
					view.setLayoutParams(layout);
				} catch (Exception e) {
				}
			}
		}
	}

	private static class UserIconDisplayListener extends SimpleImageLoadingListener {

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				imageView.setImageBitmap(ImageUtils.getRoundBitmap(loadedImage));
				addImage(imageUri);
				// }

				int imageHeight = loadedImage.getHeight();
				int imageWidth = loadedImage.getWidth();

				try {
					LayoutParams layout = view.getLayoutParams();
					layout.height = view.getWidth() * imageHeight / imageWidth;
					layout.width = view.getWidth();
					float dd = view.getContext().getResources().getDisplayMetrics().density;
					if (layout.height == 0 && layout.width == 0) {
						float scale = view.getContext().getResources().getDisplayMetrics().density;
						layout.height = (int) (96 * scale);
						layout.width = (int) (96 * scale);
					}
					view.setLayoutParams(layout);
				} catch (Exception e) {
				}
			}
		}
	}

	private static class CircleImageListener extends SimpleImageLoadingListener {

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				imageView.setImageBitmap(ImageUtils.getRoundBitmap(loadedImage));
				addImage(imageUri);
				try {
					LayoutParams layout = view.getLayoutParams();
					float scale = view.getContext().getResources().getDisplayMetrics().density;
					layout.height = (int) (64 * scale + 0.5f);
					layout.width = (int) (64 * scale + 0.5f);
					view.setLayoutParams(layout);
				} catch (Exception e) {
				}
			}
		}
	}

	private static class CommentsUserImageListener extends SimpleImageLoadingListener {

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				imageView.setImageBitmap(ImageUtils.getRoundBitmap(loadedImage));
				addImage(imageUri);
				try {
					LayoutParams layout = view.getLayoutParams();
					float scale = view.getContext().getResources().getDisplayMetrics().density;
					layout.height = (int) (48 * scale + 0.5f);
					layout.width = (int) (48 * scale + 0.5f);
					view.setLayoutParams(layout);
				} catch (Exception e) {
				}
			}
		}
	}

	private static class MyTagDisplayListener extends SimpleImageLoadingListener {

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
//				ImageView imageView = (ImageView) view;
				// boolean firstDisplay = !displayedImages.contains(imageUri);
				// if (firstDisplay) {
//				FadeInBitmapDisplayer.animate(imageView, 500);
//				addImage(imageUri);
				// }

//				int imageHeight = loadedImage.getHeight();
//				int imageWidth = loadedImage.getWidth();
//
//				try {
//					LayoutParams layout = view.getLayoutParams();
//					layout.width = view.getHeight() * imageWidth / imageHeight;
//					layout.height = view.getHeight();
//					view.setLayoutParams(layout);
//				} catch (Exception e) {
//				}
			}
		}
	}

	public static void displayMyTagImage(String imageUrl, ImageView imageView) {
		imageLoader.displayImage(imageUrl, imageView, options, myTagImageDisplayListener);
	}

	public static void displayUserIconImage(final String imageUrl, final ImageView imageView) {
		// Log.v("", "imageUrl="+imageUrl);
		// new Handler().postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		imageLoader.displayImage(imageUrl, imageView, options, userIconImageDisplayListener);
		// }
		// }, 200);
	}

	private static ImageSize getImageSize(String imageUrl) {
		String lines[] = imageUrl.split("/");
		if (lines.length > 1) {
			String fileName = lines[lines.length - 1];
			String fileNameItem[] = fileName.split("_");
			if (fileNameItem.length > 1) {
				String fileNameItem1 = fileNameItem[fileNameItem.length - 1];
				String fileNameItem2[] = fileNameItem1.split("\\.");
				if (fileNameItem2.length > 1) {
					String sizeString = fileNameItem2[0];
					String fileNameItem3[] = sizeString.split("x");
					if (fileNameItem3.length > 1) {
						return new ImageSize(Integer.valueOf(fileNameItem3[0]), Integer.valueOf(fileNameItem3[1]));
					}
				}
			}
		}
		return null;
	}

	public static void cancelDisplayImage(ImageView imageView) {
		imageLoader.cancelDisplayTask(imageView);
	}

	public static void displayImage(String imageUrl, ImageView imageView) {
		// ImageSize mImageSize = getImageSize(imageUrl);
		// ImageViewAware imageViewAware=new ImageViewAware(imageView);
		// if (mImageSize != null) {
		// imageLoader.loadImage(imageUrl, mImageSize, options,
		// animateFirstListener);
		// }
		// else {
		imageLoader.displayImage(imageUrl, imageView, options, animateFirstListener);
		// }
	}

	public static void displayContentDetailImage(String imageUrl, ImageView imageView) {
		imageLoader.displayImage(imageUrl, imageView, options, contentDetailImageDisplayListener);
	}

	public static void displayCommentsUserlImage(String imageUrl, ImageView imageView) {
		imageLoader.displayImage(imageUrl, imageView, options, commentsUserDisplayListener);
	}

	public static void displayCircleImage(String imageUrl, ImageView imageView) {
		imageLoader.displayImage(imageUrl, imageView, options, circleImageDisplayListener);
	}
	
	// public static void cancelImageLoading(){
	// imageLoader.stop();
	// }
}

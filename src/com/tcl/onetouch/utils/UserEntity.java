package com.tcl.onetouch.utils;

import org.json.JSONObject;

import com.tcl.onetouch.provider.TABLE_USER;

public class UserEntity {
	private JSONObject jsonObject;

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public UserEntity(JSONObject jsonObject) {
		super();
		this.jsonObject = jsonObject;
	}

	public String getNickName() {
		return jsonObject.optString(TABLE_USER.NICKNAME.name());
	}

	public String getName() {
		return jsonObject.optString(TABLE_USER.USERNAME.name());
	}
	
	public String getImage() {
		return jsonObject.optString(TABLE_USER.IMAGE.name());
	}

}

package com.tcl.onetouch.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.LexiconListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechUnderstander;
import com.iflytek.cloud.SpeechUnderstanderListener;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.UnderstanderResult;
import com.iflytek.cloud.ui.RecognizerDialog;
import com.iflytek.cloud.ui.RecognizerDialogListener;
import com.iflytek.cloud.util.UserWords;

public class VoiceRecognize implements RecognizerListener, SpeechUnderstanderListener {

	private final static String TAG = "VoiceRecognize";

	private Context context;
	private SpeechRecognizer speechRecognizer;
	private VoiceRecognizeListener listener;
	private RecognizerDialog iatDialog;

	private SpeechUnderstander speechUnderstander;

	public interface VoiceRecognizeListener {
		public void onRecognizeResult(String resultString);
		public void onRecognizeError(SpeechError error);

	}

	public VoiceRecognize(Context context, VoiceRecognizeListener listener) {
		super();
		this.context = context;
		this.listener = listener;
		initSpeechRecognizer();
		initSpeechUnderstander();
		initRecognizerDialog();
		upLoadUserWords();
	}

	private void initRecognizerDialog() {
		iatDialog = new RecognizerDialog(context, new InitListener() {

			@Override
			public void onInit(int arg0) {
				// TODO Auto-generated method stub

			}
		}); // 2.设置听写参数,同上节
		// 3.设置回调接口
		iatDialog.setListener(new RecognizerDialogListener() {

			@Override
			public void onResult(RecognizerResult arg0, boolean arg1) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onError(SpeechError arg0) {
				// TODO Auto-generated method stub

			}
		}); // 4.开始听写
	}

	// 初始化讯飞语音识别
	private void initSpeechRecognizer() {
		SpeechUtility.createUtility(context, SpeechConstant.APPID + "=552562ec");
		speechRecognizer = SpeechRecognizer.createRecognizer(context, null);
		speechRecognizer.setParameter(SpeechConstant.DOMAIN, "iat");
		speechRecognizer.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
		speechRecognizer.setParameter(SpeechConstant.ACCENT, "mandarin ");

		// mIat.setParameter(SpeechConstant.DOMAIN, "iat");
		// mIat.setParameter(SpeechConstant.RESULT_TYPE, "json");
		// mIat.setParameter(SpeechConstant.NLP_VERSION, "2.0");
		// mIat.setParameter(SpeechConstant.PARAMS , "sch=1");
	}

	private void initSpeechUnderstander() {
		speechUnderstander = SpeechUnderstander.createUnderstander(context, null);
		speechUnderstander.setParameter(SpeechConstant.DOMAIN, "iat");
		speechUnderstander.setParameter(SpeechConstant.NLP_VERSION, "2.0");
		speechUnderstander.setParameter(SpeechConstant.RESULT_TYPE, "json");
	}

	// 上传用户词表
	private void upLoadUserWords() {
		UserWords userWords = new UserWords();
		try {
			InputStreamReader inputReader = new InputStreamReader(context.getAssets().open("restaurant_updated.txt"));
			BufferedReader bufReader = new BufferedReader(inputReader);
			String line = "";
			while ((line = bufReader.readLine()) != null) {
				if (line.length() > 0) {
					userWords.putWord(line);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		speechRecognizer.setParameter(SpeechConstant.TEXT_ENCODING, "utf-8");
		speechRecognizer.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
		int ret = speechRecognizer.updateLexicon("userword", userWords.toString(), new LexiconListener() {
			@Override
			public void onLexiconUpdated(String lexiconId, SpeechError error) {
				if (error != null) {
					Log.d(TAG, error.toString());
				} else {
					Log.d(TAG, "上传成功!");
				}
			}
		});
		if (ret != ErrorCode.SUCCESS) {
			Log.d(TAG, "上传用户词表失败:" + ret);
		}
	}

	@Override
	public void onBeginOfSpeech() {

	}

	@Override
	public void onEndOfSpeech() {
//		iatDialog.show();
	}

	@Override
	public void onError(SpeechError error) {
		if (listener != null) {
			listener.onRecognizeError(error);
		}
	}

	@Override
	public void onEvent(int arg0, int arg1, int arg2, Bundle arg3) {

	}

	@Override
	public void onResult(RecognizerResult arg0, boolean arg1) {
		if (listener != null) {
			listener.onRecognizeResult(arg0.getResultString());
		}
		iatDialog.hide();
	}

	@Override
	public void onVolumeChanged(int arg0) {

	}

	public void startListening() {
		// speechRecognizer.startListening(this);
		speechUnderstander.startUnderstanding(this);
	}

	public void stopListening() {
		// speechRecognizer.stopListening();
		speechUnderstander.stopUnderstanding();
	}

	public boolean isListening() {
		// return speechRecognizer.isListening();
		return speechUnderstander.isUnderstanding();
	}

	@Override
	public void onResult(UnderstanderResult arg0) {
		if (listener != null) {
			listener.onRecognizeResult(arg0.getResultString());
		}
	}

	public void startSpeechUnderstander() {
		speechUnderstander.startUnderstanding(this);
	}

	public void stopSpeechUnderstander() {
		speechUnderstander.stopUnderstanding();
	}

}

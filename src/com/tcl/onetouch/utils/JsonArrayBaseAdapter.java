package com.tcl.onetouch.utils;

import org.json.JSONArray;

import android.widget.BaseAdapter;

public abstract class JsonArrayBaseAdapter extends BaseAdapter {

	protected JSONArray array = new JSONArray();

	public JSONArray getArray() {
		return array;
	}

	public void removeAll() {
		array = new JSONArray();
	}

	public void addArray(JSONArray jsonArray) {
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				array.put(jsonArray.optJSONObject(i));
			}
		}
	}

	public void pushArray(JSONArray jsonArray) {
		JSONArray tmpArray = new JSONArray();

		for (int i = 0; i < jsonArray.length(); i++) {
			tmpArray.put(jsonArray.opt(i));
		}

		for (int i = 0; i < array.length(); i++) {
			tmpArray.put(array.opt(i));
		}

		array = tmpArray;
	}

	@Override
	public int getCount() {
		return array == null ? 0 : array.length();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}
}

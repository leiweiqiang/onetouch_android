package com.tcl.onetouch.utils;

import org.json.JSONObject;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.event.RestaurantSelectEvent;

import de.greenrobot.event.EventBus;

public class AddShareClass extends ImageView {

	private boolean isAddShare = false;
	private JSONObject jsonObject;

	public AddShareClass(Context context, AttributeSet attrs) {
		super(context, attrs);
//		setText(getResources().getString(R.string.booking));
//		setBackgroundResource(R.drawable.share_button_background);
		updateStatus();
		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				doAnimation(view);
			}
		});
	}
	
	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject, boolean isSelect) {
		this.jsonObject = jsonObject;
		isAddShare = isSelect;
		updateStatus();
	}

	protected void doAnimation(final View view) {
		Animation scaleAnimation = new ScaleAnimation(1f, 1.05f, 1f, 1.05f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		scaleAnimation.setDuration(100);

		scaleAnimation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				Animation scaleAnimation = new ScaleAnimation(1.05f, 1.0f, 1.05f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				scaleAnimation.setDuration(100);
				scaleAnimation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {

					}

					@Override
					public void onAnimationRepeat(Animation animation) {

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						isAddShare = !isAddShare;
						EventBus.getDefault().post(new RestaurantSelectEvent(jsonObject, isAddShare));
						updateStatus();
					}
				});
				view.startAnimation(scaleAnimation);
			}
		});

		view.startAnimation(scaleAnimation);
	}
	
	private void updateStatus(){
		setImageResource(isAddShare?R.drawable.select: R.drawable.unselect);
//		setText(getResources().getString(isAddShare ? R.string.cancel_share : R.string.add_share));
//		setBackgroundResource(isAddShare ? R.drawable.cancel_share_button_background : R.drawable.share_button_background);

	}

}

package com.tcl.onetouch.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.view.View;

public class ImageUtils {

	private final Paint bitmapPaint;
	private final Matrix matrix;
	private final RectF source;
	private final RectF destination;

	public ImageUtils() {
		bitmapPaint = new Paint();
		bitmapPaint.setAntiAlias(true);
		bitmapPaint.setFilterBitmap(true);
		bitmapPaint.setDither(true);

		matrix = new Matrix();
		source = new RectF();
		destination = new RectF();
	}

	public void drawBitmapWithCircleOnCanvas(Bitmap bitmap, Canvas canvas, View view) {
		source.set(0, 0, bitmap.getWidth(), bitmap.getHeight());
		destination.set(0, 0, view.getWidth(), view.getHeight());

		BitmapShader shader = new BitmapShader(bitmap, TileMode.CLAMP, TileMode.CLAMP);
		matrix.reset();

		matrix.setRectToRect(source, destination, Matrix.ScaleToFit.FILL);
		shader.setLocalMatrix(matrix);

		bitmapPaint.setShader(shader);
		canvas.drawCircle(destination.centerX(), destination.centerY(), destination.width() / 2f, bitmapPaint);
	}

	public static Bitmap geUptRoundCornerBitmap(Bitmap bitmap, int radius) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Bitmap output = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, w, h);
		final RectF rectF = new RectF(rect);
		final float roundPx = radius;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		final Rect rectTop = new Rect(0, radius, w, h);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
		paint.setColor(color);
		canvas.drawRect(rectTop, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}

	public static Bitmap getRoundBitmap(Bitmap bitmap) {

		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.RGB_565);
		Canvas canvas = new Canvas(output);

		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		canvas.drawRoundRect(rectF, bitmap.getWidth() / 2, bitmap.getHeight() / 2, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}
}
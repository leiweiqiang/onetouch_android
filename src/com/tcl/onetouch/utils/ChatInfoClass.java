package com.tcl.onetouch.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class ChatInfoClass {

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public JSONObject getLocation() {
		return location;
	}

	public void setLocation(JSONObject location) {
		this.location = location;
	}

	private Date time;
	private JSONObject location;
	private JSONArray selectedRestaurants;
	boolean isNotification = false;

	public boolean isNotification() {
		return isNotification;
	}

	public void setNotification(boolean isNotification) {
		this.isNotification = isNotification;
	}

	public JSONArray getSelectedRestaurants() {
		return selectedRestaurants;
	}

	public void setSelectedRestaurants(JSONArray selectedRestaurants) {
		this.selectedRestaurants = selectedRestaurants;
	}

	private String category;

	public String getCategory() {
		return category.equals("null") ? "����" : category;
		// return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	private UserEntity sendFrom;
	private UserEntity sendTo;

	public ChatInfoClass(String chatInfoString) {
		super();

		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(chatInfoString);
			setDate(jsonObject.optString("time"));
			location = jsonObject.optJSONObject("location");
			category = jsonObject.optString("category");
			isNotification = jsonObject.optBoolean("isNotification");
			sendFrom = new UserEntity(jsonObject.optJSONObject("sendFrom"));
			sendTo = new UserEntity(jsonObject.optJSONObject("sendTo"));
			selectedRestaurants = jsonObject.optJSONArray("selectedRestaurants");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void setDate(String timeString) {
		if (!timeString.equals("null")) {
			if (TextUtils.isDigitsOnly(timeString)) {
				long time_value = Long.parseLong(timeString);
				time = new Date(time_value);
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
				try {
					time = simpleDateFormat.parse(timeString);
				} catch (ParseException e) {
					time = new Date();
					e.printStackTrace();
				}
			}
		} else {
			time = new Date();
		}
	}

	public JSONObject toJsonObject() {
		JSONObject object = new JSONObject();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		try {
			object.put("time", simpleDateFormat.format(time));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			object.put("location", location);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			object.put("category", category);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			object.put("isNotification", isNotification);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			object.put("sendFrom", sendFrom.getJsonObject());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			object.put("sendTo", sendTo.getJsonObject());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			object.put("selectedRestaurants", selectedRestaurants);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			object.put("timeStamp", new Date().getTime());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object;
	}

	public UserEntity getSendTo() {
		return !isNotification ? sendTo : sendFrom;
	}

	public void setSendTo(UserEntity sendTo) {
		this.sendTo = sendTo;
	}

	public UserEntity getSendFrom() {
		return !isNotification ? sendFrom : sendTo;
	}

	public void setSendFrom(UserEntity sendFrom) {
		this.sendFrom = sendFrom;
	}

	public String getLocationName() {
		return location.optString("name");
	}

	public String getTimeShortString() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd HH:mm", Locale.getDefault());
		return simpleDateFormat.format(time);
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	public Calendar getCalendar() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(time);
		return calendar;
	}

}

package com.tcl.onetouch.utils;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class VolleyRequestModule {
	private static VolleyRequestModule instance = null;
	private static RequestQueue requestQueue;
	private static ImageLoader imageLoader;
//	private static ImageUtils imageUtils;
	
	public static VolleyRequestModule getInstance(Context context) {
		if (instance == null) {
			instance = new VolleyRequestModule();
			requestQueue = Volley.newRequestQueue(context);
			requestQueue.start();
//			imageLoader = new ImageLoader(requestQueue, BitmapLruCache.newInstance(context));
//			imageUtils = new ImageUtils();
		}
		return instance;
	}
	
	public static RequestQueue getRequestQueue() {
		return requestQueue;
	}

	public static ImageLoader getImageLoader() {
		return imageLoader;
	}
	
//	public static ImageUtils getImageUtils() {
//		return imageUtils;
//	}
}

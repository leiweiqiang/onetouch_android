package com.tcl.onetouch.utils;

import org.json.JSONObject;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.event.BookingEvent;

import de.greenrobot.event.EventBus;

public class BookingClass extends TextView {

	private boolean isAddShare = false;
	private JSONObject jsonObject;
	private Context context;

	public BookingClass(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		setText(getResources().getString(R.string.booking));
		setBackgroundResource(R.drawable.share_button_background);

		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				doAnimation(view);
			}
		});
	}
	
	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject, boolean isSelect) {
		this.jsonObject = jsonObject;
		isAddShare = isSelect;
		updateStatus();
	}

	protected void doAnimation(final View view) {
		Animation scaleAnimation = new ScaleAnimation(1f, 1.05f, 1f, 1.05f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		scaleAnimation.setDuration(100);

		scaleAnimation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {

			}

			@Override
			public void onAnimationRepeat(Animation arg0) {

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				Animation scaleAnimation = new ScaleAnimation(1.05f, 1.0f, 1.05f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				scaleAnimation.setDuration(500);
				scaleAnimation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation arg0) {

					}

					@Override
					public void onAnimationRepeat(Animation arg0) {

					}

					@Override
					public void onAnimationEnd(Animation arg0) {
						isAddShare = !isAddShare;
//						saveBookingMessage();
						EventBus.getDefault().post(new BookingEvent(jsonObject));
//						updateStatus();
					}
				});
				view.startAnimation(scaleAnimation);
			}
		});

		view.startAnimation(scaleAnimation);
	}
	
	private void updateStatus(){
//		setText(getResources().getString(isAddShare ? R.string.cancel_share : R.string.add_share));
//		setBackgroundResource(isAddShare ? R.drawable.cancel_share_button_background : R.drawable.share_button_background);
	}

}

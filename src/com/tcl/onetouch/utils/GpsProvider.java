package com.tcl.onetouch.utils;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

public class GpsProvider {
	
	private static GpsProvider instance = null;
	private static Context context;
	private static LocationManager locationManager;
	private static Location currentLocation;
	
	public static GpsProvider getInstance(Context ctx) {
		context = ctx;
		if (instance == null) {
			instance = new GpsProvider();
			initGps();
		}
		return instance;
	}
	
	public static Location getCurrentLocation(){
		return currentLocation;
	}
	
	private static void initGps() {
		locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);

		// 根据设置的Criteria对象，获取最符合此标准的provider对象
		String gpsProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER).getName();
		String networdProvider = locationManager.getProvider(LocationManager.NETWORK_PROVIDER).getName();
		// 根据当前provider对象获取最后一次位置信息
		currentLocation = locationManager.getLastKnownLocation(gpsProvider);
		if (currentLocation == null) {
			currentLocation = locationManager.getLastKnownLocation(networdProvider);
		}
		// 如果位置信息为null，则请求更新位置信息
		// if(currentLocation == null){
		locationManager.requestLocationUpdates(gpsProvider, 0, 0, locationListener);
		locationManager.requestLocationUpdates(networdProvider, 0, 0, locationListener);

		// }
		// 增加GPS状态监听器
		// locationManager.addGpsStatusListener(gpsListener);
	}

	private static LocationListener locationListener = new LocationListener() {
		// 位置发生改变时调用
		@Override
		public void onLocationChanged(Location location) {
			Log.d("Location", "onLocationChanged");
			currentLocation = location;
		}

		// provider失效时调用
		@Override
		public void onProviderDisabled(String provider) {
			Log.d("Location", "onProviderDisabled");
		}

		// provider启用时调用
		@Override
		public void onProviderEnabled(String provider) {
			Log.d("Location", "onProviderEnabled");
		}

		// 状态改变时调用
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.d("Location", "onStatusChanged");
		}
	};


	public static final boolean isOpen(final Context context) {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		// 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
		boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
		boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		if (gps || network) {
			return true;
		}

		return false;
	}

	public static final void openGPS(Context context) {
		Intent GPSIntent = new Intent();
		GPSIntent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
		GPSIntent.addCategory("android.intent.category.ALTERNATIVE");
		GPSIntent.setData(Uri.parse("custom:3"));
		try {
			PendingIntent.getBroadcast(context, 0, GPSIntent, 0).send();
		} catch (CanceledException e) {
			e.printStackTrace();
		}
	}

	public static final void openGPS1(Context context) {
		Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
		intent.putExtra("enabled", true);
		context.sendBroadcast(intent);
		String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			context.sendBroadcast(poke);
		}
	}

	public static final void openLocationSetting(final Context context) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).setTitle("程序需要使用定位服务").setMessage("请打开手机GPS").create();
		alertDialog.setButton("打开GPS", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			}
		});
		alertDialog.setButton2("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
			}
		});
		alertDialog.show();
	}
}

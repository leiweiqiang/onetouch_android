package com.tcl.onetouch.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;

import com.tcl.onetouch.provider.TABLE_USER;

public class UserData {

	private static UserData instance = null;
	private static Context context;

	public static String SHARED_PREFERENCES_FILE_NAME = "data";

	public enum USERDATA_FIELD {
		USERNAME, //
		PASSWORD
	}

	public static UserData getInstance(Context ctx) {
		context = ctx;
		if (instance == null) {
			instance = new UserData();
		}
		return instance;
	}

	public String getStringData(USERDATA_FIELD field) {
		SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		return preferences.getString(field.name(), "");
	}

	public void setStringData(USERDATA_FIELD field, String value) {
		SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putString(field.name(), value);
		editor.commit();
	}

	public JSONObject getUserByUserName(String username) {
		JSONObject jsonObject = null;

		Cursor cursor = context.getContentResolver().query(TABLE_USER.CONTENT_URI, null, TABLE_USER.USERNAME.name() + "=?", new String[] { username }, null);

		if (cursor.moveToNext()) {
			jsonObject = new JSONObject();
			try {
				jsonObject.put(TABLE_USER.USERNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.USERNAME.name())));
				jsonObject.put(TABLE_USER.NICKNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.NICKNAME.name())));
				jsonObject.put(TABLE_USER.IMAGE.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.IMAGE.name())));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		cursor.close();
		return jsonObject;
	}

	public JSONObject getOtherUser(String nickname) {
		JSONObject jsonObject = null;

		Cursor cursor = context.getContentResolver().query(TABLE_USER.CONTENT_URI, null, TABLE_USER.NICKNAME.name() + "<>?", new String[] { nickname }, null);

		if (cursor.moveToNext()) {
			jsonObject = new JSONObject();
			try {
				jsonObject.put(TABLE_USER.USERNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.USERNAME.name())));
				jsonObject.put(TABLE_USER.NICKNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.NICKNAME.name())));
				jsonObject.put(TABLE_USER.IMAGE.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.IMAGE.name())));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		cursor.close();
		return jsonObject;
	}

	public JSONObject getUserByNickName(String nickname) {
		JSONObject jsonObject = null;

		Cursor cursor = context.getContentResolver().query(TABLE_USER.CONTENT_URI, null, TABLE_USER.NICKNAME.name() + " like ?", new String[] { nickname }, null);

		if (cursor.moveToNext()) {
			jsonObject = new JSONObject();
			try {
				jsonObject.put(TABLE_USER.USERNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.USERNAME.name())));
				jsonObject.put(TABLE_USER.NICKNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.NICKNAME.name())));
				jsonObject.put(TABLE_USER.IMAGE.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.IMAGE.name())));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		cursor.close();
		return jsonObject;
	}

	public JSONObject getFromUser() {
		String username = getStringData(USERDATA_FIELD.USERNAME);
		return getUserByUserName(username);
	}

	public JSONObject getToUser(JSONObject user) {
		JSONObject fromUser = getFromUser();
		if (user != null) {
			if (fromUser.optString(TABLE_USER.NICKNAME.name()).equals(user.optString("nickname"))) {
				return getOtherUser(user.optString("nickname"));
			} else {
				JSONObject jsonObject = getUserByNickName(user.optString("nickname"));
				if (jsonObject == null) {
					jsonObject = getOtherUser(fromUser.optString(TABLE_USER.NICKNAME.name()));
				}
				return jsonObject;
			}
		}
		else {
			return  getOtherUser(fromUser.optString(TABLE_USER.NICKNAME.name()));
		}

	}

	public JSONArray getUserArray() {
		JSONObject fromUser = getFromUser();
		JSONArray jsonArray = new JSONArray();
		Cursor cursor = context.getContentResolver().query(TABLE_USER.CONTENT_URI, null, TABLE_USER.NICKNAME.name() + "<>?", new String[] { fromUser.optString(TABLE_USER.NICKNAME.name()) }, null);

		while (cursor.moveToNext()) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put(TABLE_USER.USERNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.USERNAME.name())));
				jsonObject.put(TABLE_USER.NICKNAME.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.NICKNAME.name())));
				jsonObject.put(TABLE_USER.IMAGE.name(), cursor.getString(cursor.getColumnIndex(TABLE_USER.IMAGE.name())));
				jsonArray.put(jsonObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		cursor.close();
		return jsonArray;
	}

}

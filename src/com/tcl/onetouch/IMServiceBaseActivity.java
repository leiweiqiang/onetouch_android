package com.tcl.onetouch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import com.tcl.onetouch.service.IMService;

public abstract class IMServiceBaseActivity extends Activity {
	protected static final String IS_SHOW_ALERT_FLAG = "IS_SHOW_ALERT_FLAG";

	protected IMService imService = null;
	public ServiceConnection serviceConnection = null;

	abstract protected void onIMServiceConnected();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		binderService();
		super.onCreate(savedInstanceState);
	}

	public boolean isIntentFromNotification() {
		return getIntent().getBooleanExtra(IS_SHOW_ALERT_FLAG, false);
	}

	private void binderService() {
		Intent intent = new Intent(this, IMService.class);
		serviceConnection = new ServiceConnection() {
			@Override
			public void onServiceConnected(ComponentName componentName, IBinder binder) {
				imService = ((IMService.LocalBinder) binder).getService();
				onIMServiceConnected();
			}

			@Override
			public void onServiceDisconnected(ComponentName componentName) {
				imService = null;
			}
		};
		bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}
	
	private void showConflitDialog(Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle("用户已在其它地点登陆");
		builder.setNeutralButton("退出", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				System.exit(0);
			}
		});
		builder.setPositiveButton("重新登陆", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
		builder.show();
	}

//	@Override
//	public void finish() {
//		unbindService(serviceConnection);
//		super.finish();
//	}

}

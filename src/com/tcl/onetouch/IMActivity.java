package com.tcl.onetouch;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.iflytek.cloud.SpeechError;
import com.tcl.onetouch.event.BookedEvent;
import com.tcl.onetouch.event.BookingEvent;
import com.tcl.onetouch.im.ChatMsgViewAdapter;
import com.tcl.onetouch.provider.TABLE_KEYWORD;
import com.tcl.onetouch.provider.TABLE_MESSAGE;
import com.tcl.onetouch.service.IMService.SendImMessageListener;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.UserData;
import com.tcl.onetouch.utils.UserData.USERDATA_FIELD;
import com.tcl.onetouch.utils.VoiceRecognize;
import com.tcl.onetouch.utils.VoiceRecognize.VoiceRecognizeListener;
import com.tcl.onetouch.view.ChatInfoView;
import com.tcl.onetouch.view.TagItemView;

import de.greenrobot.event.EventBus;

public class IMActivity extends IMServiceBaseActivity implements VoiceRecognizeListener, SendImMessageListener {
	private final class KeywordsObserver extends ContentObserver {
		public KeywordsObserver(Handler handler) {
			super(handler);
		}

		@Override
		public void onChange(boolean selfChange) {
			updateTagView();
		}
	}

	private final class MessageObserver extends ContentObserver {
		public MessageObserver(Handler handler) {
			super(handler);
		}

		@Override
		public void onChange(boolean selfChange) {
			mAdapter.notifyDataSetChanged();
			handler.sendEmptyMessageDelayed(MESSAGE_UPDATE, 500);
		}
	}

	private ChatInfoClass chatInfoClass;

	private ChatInfoView chatInfoView;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MESSAGE_UPDATE:
				scrollMyListViewToBottom();
				break;

			case MESSAGE_SEND_SUCCESS:
				EditText msg_content = (EditText) findViewById(R.id.msg_content);
				msg_content.setText("");
				break;

			case MESSAGE_SEND_FAILED:
				Toast.makeText(getApplicationContext(), "消息发送失败", Toast.LENGTH_SHORT).show();
				break;

			default:
				break;
			}
		}
	};
	private KeywordsObserver keywordsObserver = new KeywordsObserver(new Handler());
	private ChatMsgViewAdapter mAdapter;
	private final int MESSAGE_UPDATE = 0;
	private final int MESSAGE_SEND_SUCCESS = 1;
	private final int MESSAGE_SEND_FAILED = 2;

	private MessageObserver messageObserver = new MessageObserver(new Handler());

	private ListView mListView;

	private VoiceRecognize voiceRecognize;

	@Override
	public void finish() {
		getContentResolver().unregisterContentObserver(messageObserver);
		getContentResolver().unregisterContentObserver(keywordsObserver);
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.finish();
	}

	public void onBookedEvent(final BookedEvent event) {
		ImageView imageView = (ImageView) findViewById(R.id.booked_icon);
		imageView.setImageResource(R.drawable.dingwei_active);
	}

	public void onBookingEvent(final BookingEvent event) {
		saveBookingMessage(chatInfoClass.getSendFrom().getName(), chatInfoClass.getSendTo().getName());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_im);

		String chatInfoString = getIntent().getStringExtra("chatInfoClass");
		chatInfoClass = new ChatInfoClass(chatInfoString);

		chatInfoView = (ChatInfoView) findViewById(R.id.chat_info_view);
		chatInfoView.setChatInfoClass(chatInfoClass);
		chatInfoView.disableUserClick();
		chatInfoView.setIMActivity(true);

		voiceRecognize = new VoiceRecognize(this, this);

		findViewById(R.id.press_to_talk).setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {
				TextView textView = (TextView) view;

				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {
					textView.setText(getResources().getString(R.string.voice_recognizing));
					voiceRecognize.startListening();
				}
					break;

				case MotionEvent.ACTION_UP: {
					textView.setText(getResources().getString(R.string.press_talk));
					voiceRecognize.stopListening();
				}
					break;

				default:
					break;
				}
				return true;
			}
		});

		ImageView mic_image = (ImageView) findViewById(R.id.mic_image);
		mic_image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				ImageView mic_image = (ImageView) view;
				if (mic_image.getTag() == null) {
					mic_image.setTag("1");
					mic_image.setImageResource(R.drawable.micphone);
					findViewById(R.id.press_to_talk).setVisibility(View.VISIBLE);
					findViewById(R.id.msg_content).setVisibility(View.GONE);
				} else {
					mic_image.setTag(null);
					mic_image.setImageResource(R.drawable.keyboard);
					findViewById(R.id.press_to_talk).setVisibility(View.GONE);
					findViewById(R.id.msg_content).setVisibility(View.VISIBLE);
				}
			}
		});

		EditText msg_content = (EditText) findViewById(R.id.msg_content);
		msg_content.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEND) {
					EditText msg_content = (EditText) findViewById(R.id.msg_content);
					imService.sendImMessage(chatInfoClass, msg_content.getText().toString(), IMActivity.this);
				}
				return false;
			}
		});

		mListView = (ListView) findViewById(R.id.chat_listView);

		LinearLayout footer = new LinearLayout(this);
		footer.setLayoutParams(new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, 96));

		mListView.addFooterView(footer);

		mAdapter = new ChatMsgViewAdapter(this, chatInfoClass);
		mListView.setAdapter(mAdapter);
		getContentResolver().registerContentObserver(TABLE_MESSAGE.CONTENT_URI, true, messageObserver);
		getContentResolver().registerContentObserver(TABLE_KEYWORD.CONTENT_URI, true, keywordsObserver);

		scrollMyListViewToBottom();

		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onBookingEvent", BookingEvent.class);
			EventBus.getDefault().register(this, "onBookedEvent", BookedEvent.class);
		}
		updateTagView();
	}

	@Override
	protected void onIMServiceConnected() {

	}

	@Override
	public void onRecognizeError(SpeechError error) {
		Toast.makeText(getApplicationContext(), "请您再说一次", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onRecognizeResult(String resultString) {
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(resultString);
			imService.sendImMessage(chatInfoClass, jsonObject.optString("text"), IMActivity.this);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSendImMessageFailed() {
		handler.sendEmptyMessage(MESSAGE_SEND_FAILED);
	}

	@Override
	public void onSendImMessageSuccess() {
		handler.sendEmptyMessage(MESSAGE_SEND_SUCCESS);
	}

	private void saveBookingMessage(String from, String to) {
		ContentValues values = new ContentValues();
		values.put(TABLE_MESSAGE.USER.name(), UserData.getInstance(this).getStringData(USERDATA_FIELD.USERNAME));
		values.put(TABLE_MESSAGE.MSG_FROM.name(), chatInfoClass.getSendFrom().getName());
		values.put(TABLE_MESSAGE.MSG_TO.name(), to);
		values.put(TABLE_MESSAGE.DATE.name(), "" + new Date().getTime());
		values.put(TABLE_MESSAGE.MSG_CONTENT.name(), "");
		values.put(TABLE_MESSAGE.SUBJECT.name(), "booking");
		values.put(TABLE_MESSAGE.IS_INCOMMING_MSG.name(), "false");
		Uri uri = getContentResolver().insert(TABLE_MESSAGE.CONTENT_URI, values);
	}

	private void scrollMyListViewToBottom() {
		mListView.post(new Runnable() {
			@Override
			public void run() {
				mListView.setSelection(mAdapter.getCount() - 1);
			}
		});
	}

	private void updateTagView() {
		LinearLayout tagLayout = (LinearLayout) findViewById(R.id.tag_layout);
		tagLayout.removeAllViews();
		Cursor cursor = getContentResolver().query(TABLE_KEYWORD.CONTENT_URI, null, null, null, null);
		findViewById(R.id.tag).setVisibility(cursor.getCount() > 0 ? View.VISIBLE : View.GONE);
		while (cursor.moveToNext()) {
			String tagName = cursor.getString(TABLE_KEYWORD.KEYWORD.ordinal());
			TagItemView tagItemView = new TagItemView(this, chatInfoClass);
			tagItemView.setText(tagName);
			tagLayout.addView(tagItemView);
		}
		cursor.close();
	}

}

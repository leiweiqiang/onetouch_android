package com.tcl.onetouch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.baidu.mapapi.SDKInitializer;
import com.iflytek.cloud.SpeechError;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.tcl.onetouch.api.ContactModel;
import com.tcl.onetouch.api.RideoJsonArrayRequest.RideoJsonArrayRequestListener;
import com.tcl.onetouch.provider.TABLE_USER;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.GpsProvider;
import com.tcl.onetouch.utils.UserData;
import com.tcl.onetouch.utils.VoiceRecognize;
import com.tcl.onetouch.utils.VoiceRecognize.VoiceRecognizeListener;
import com.tcl.onetouch.utils.VolleyRequestModule;

public class MainActivity extends IMServiceBaseActivity implements VoiceRecognizeListener {

	private enum VOICE_STATE {
		VOICE_STATE_WAITTING, //
		VOICE_STATE_LISTENING, //
		VOICE_STATE_RECOGNIZE_SUCCESS, //
		VOICE_STATE_RECOGNIZE_FAILED
	}

	private final String ACTION_NEW_RESTAURANT_CHAT = "NEW_RESTAURANT_CHAT";
	private final String ACTION_XMPP_USER_CONFLIT = "ACTION_XMPP_USER_CONFLIT";

	private final int MESSAGE_VOICE_RECOGNIZE_SUCCESS = 100;
	private final int MESSAGE_RESTART_APP = 101;

	private VoiceRecognize voiceRecognize;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_VOICE_RECOGNIZE_SUCCESS:
				setState(VOICE_STATE.VOICE_STATE_WAITTING);
				openRecommendActivity((String) msg.obj);
				break;

			case MESSAGE_RESTART_APP: {
				final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
				break;

			default:
				break;
			}

		}
	};

	private VOICE_STATE currentState = VOICE_STATE.VOICE_STATE_WAITTING;

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_NEW_RESTAURANT_CHAT)) {
				try {
					showNotification(MainActivity.this, intent.getStringExtra("msgJsonObject"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else if (action.equals(ACTION_XMPP_USER_CONFLIT)) {
				unbindService(serviceConnection);
				handler.sendEmptyMessageDelayed(MESSAGE_RESTART_APP, 1000);
			}
		}

	};

	private boolean isExit = false;

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			isExit = false;
		}
	};

	public void exit() {
		if (!isExit) {
			isExit = true;
			Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
			mHandler.sendEmptyMessageDelayed(0, 2000);
		} else {
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			startActivity(intent);
			System.exit(0);
		}
	}

	@Override
	public void finish() {
		unregisterReceiver(mBroadcastReceiver);
		super.finish();
	}

	private void getContacts() {
		ContactModel model = new ContactModel(new RideoJsonArrayRequestListener() {

			@Override
			public void onDataError(VolleyError error) {

			}

			@Override
			public void onDataRecv(JSONArray jsonArray) {
				for (int i = 0; i < jsonArray.length(); i++) {
					saveUser(jsonArray.optJSONObject(i));
				}
			}
		});
		model.fetch();
	}

	private void initControls(Context context) {
		VolleyRequestModule.getInstance(context);
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).threadPoolSize(3).threadPriority(Thread.MAX_PRIORITY).denyCacheImageMultipleSizesInMemory()
				.memoryCache(new LruMemoryCache(100 * 1024 * 1024)).memoryCacheSize(100 * 1024 * 1024).diskCacheFileNameGenerator(new Md5FileNameGenerator()).build();
		ImageLoader.getInstance().init(config);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		SDKInitializer.initialize(getApplicationContext());

		setState(VOICE_STATE.VOICE_STATE_WAITTING);
		initControls(this);
		findViewById(R.id.main_layout).setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					if (currentState == VOICE_STATE.VOICE_STATE_WAITTING) {
						voiceRecognize.startListening();
					}
					setState(VOICE_STATE.VOICE_STATE_LISTENING);
					return true;

				case MotionEvent.ACTION_UP:
					if (currentState == VOICE_STATE.VOICE_STATE_LISTENING) {
						voiceRecognize.stopListening();
					}
					return true;

				default:
					break;
				}
				return false;
			}
		});
		registerBoradcastReceiver();

		GpsProvider.getInstance(this);
		getContacts();
		voiceRecognize = new VoiceRecognize(this, this);
	}

	@Override
	protected void onIMServiceConnected() {
		if (isIntentFromNotification()) {
			showNotificationConfirm();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exit();
			return false;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public void onRecognizeError(SpeechError error) {
		setState(VOICE_STATE.VOICE_STATE_RECOGNIZE_FAILED);
	}

	@Override
	public void onRecognizeResult(String resultString) {
		setState(VOICE_STATE.VOICE_STATE_RECOGNIZE_SUCCESS);
		try {
			JSONObject jsonObject = new JSONObject(resultString);
			TextView textView = (TextView) findViewById(R.id.voice_result);
			String keywords = jsonObject.optString("text");
			textView.setText(keywords);

			Message msg = new Message();
			msg.what = MESSAGE_VOICE_RECOGNIZE_SUCCESS;
			msg.obj = keywords;
			handler.sendMessageDelayed(msg, 1 * 1000);

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void openRecommendActivity(String text) {
		Intent intent = new Intent(this, ChatPrepareActivity.class);
		intent.putExtra("keywords", text);
		startActivity(intent);
	}

	public void registerBoradcastReceiver() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(ACTION_NEW_RESTAURANT_CHAT);
		myIntentFilter.addAction(ACTION_XMPP_USER_CONFLIT);
		registerReceiver(mBroadcastReceiver, myIntentFilter);
	}

	private void saveUser(JSONObject object) {
		Cursor cursor = getContentResolver().query(TABLE_USER.CONTENT_URI, null, TABLE_USER.USERNAME + "=?", new String[] { object.optString("username") }, null);
		if (cursor.getCount() == 0) {
			ContentValues values = new ContentValues();
			values.put(TABLE_USER.USERNAME.name(), object.optString("username"));
			values.put(TABLE_USER.NICKNAME.name(), object.optString("nickname"));
			values.put(TABLE_USER.IMAGE.name(), object.optString("image"));
			Uri uri = getContentResolver().insert(TABLE_USER.CONTENT_URI, values);
		}
		cursor.close();
	}

	private void setBackGroundColor(int res) {
		findViewById(R.id.main_layout).setBackgroundColor(res);
	}

	private void setState(VOICE_STATE voiceState) {

		currentState = voiceState;

		findViewById(R.id.voice_info).setVisibility(currentState == VOICE_STATE.VOICE_STATE_WAITTING ? View.VISIBLE : View.INVISIBLE);
		findViewById(R.id.voice_mic).setVisibility(currentState == VOICE_STATE.VOICE_STATE_WAITTING ? View.VISIBLE : View.INVISIBLE);
		findViewById(R.id.voice_wave).setVisibility(currentState == VOICE_STATE.VOICE_STATE_LISTENING ? View.VISIBLE : View.INVISIBLE);
		findViewById(R.id.voice_edit).setVisibility(currentState == VOICE_STATE.VOICE_STATE_RECOGNIZE_SUCCESS ? View.VISIBLE : View.INVISIBLE);
		findViewById(R.id.voice_result).setVisibility(currentState == VOICE_STATE.VOICE_STATE_RECOGNIZE_SUCCESS ? View.VISIBLE : View.INVISIBLE);
		findViewById(R.id.no_voice).setVisibility(currentState == VOICE_STATE.VOICE_STATE_RECOGNIZE_FAILED ? View.VISIBLE : View.INVISIBLE);

		switch (currentState) {

		case VOICE_STATE_WAITTING: {
			setStateInfoText("等待中");
			setBackGroundColor(getResources().getColor(R.color.main_color));
			findViewById(R.id.voice_wave).setVisibility(View.INVISIBLE);
			View view = findViewById(R.id.voice_waitting);
			view.clearAnimation();
			view.setVisibility(View.GONE);
		}
			break;

		case VOICE_STATE_RECOGNIZE_FAILED: {
			setState(VOICE_STATE.VOICE_STATE_WAITTING);
			findViewById(R.id.no_voice).setVisibility(View.VISIBLE);
			findViewById(R.id.voice_mic).setVisibility(View.INVISIBLE);
		}
			break;

		case VOICE_STATE_RECOGNIZE_SUCCESS: {
			setStateInfoText("听懂啦");
			findViewById(R.id.voice_wave).setVisibility(View.INVISIBLE);
			View view = findViewById(R.id.voice_waitting);
			view.clearAnimation();
			view.setVisibility(View.GONE);
		}
			break;

		case VOICE_STATE_LISTENING: {
			setStateInfoText("正在认真的听");
			setBackGroundColor(getResources().getColor(R.color.second_color));

			View view = findViewById(R.id.voice_waitting);
			view.setVisibility(View.VISIBLE);
			Animation animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			animation.setDuration(1000 * 2);
			animation.setRepeatCount(1000);
			view.startAnimation(animation);
		}
			break;

		default:
			break;
		}
	}

	private void setStateInfoText(String infoString) {
		TextView textView = (TextView) findViewById(R.id.state_info);
		textView.setText(infoString);
	}

	private void showDialog(Context context, final JSONObject jsonObject) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		String fromUser = jsonObject.optString("from");
		builder.setTitle(fromUser + "邀请你加入饭局");
		builder.setNeutralButton("拒绝", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		builder.setPositiveButton("接受", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				imService.saveMessage(jsonObject);
				Intent intent = new Intent(MainActivity.this, IMActivity.class);
				ChatInfoClass chatInfoClass = new ChatInfoClass(jsonObject.optString("body"));
				chatInfoClass.setNotification(true);
				intent.putExtra("chatInfoClass", chatInfoClass.toJsonObject().toString());
				startActivity(intent);
			}
		});
		builder.show();
	}

	private void showNotification(Context context, String msgJsonObjectString) throws JSONException {

		JSONObject restChatInfoJsonObject = new JSONObject(msgJsonObjectString);

		JSONObject fromUserJsonObject = UserData.getInstance(context).getUserByUserName(restChatInfoJsonObject.optString("from"));
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new NotificationCompat.Builder(MainActivity.this).setSmallIcon(R.drawable.ic_launcher)
				.setTicker(fromUserJsonObject.optString(TABLE_USER.NICKNAME.name()) + "邀请你加入饭局").setContentTitle("晓明邀请你加入饭局")//
				.setContentText("ContentText").setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL).build();

		Intent intent = new Intent(this, NewChatAlertActivity.class);
		intent.putExtra(IS_SHOW_ALERT_FLAG, true);
		intent.putExtra("message", restChatInfoJsonObject.toString());

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		notification.defaults = Notification.DEFAULT_SOUND;

		notification
				.setLatestEventInfo(this, fromUserJsonObject.optString(TABLE_USER.NICKNAME.name()) + "邀请你加入饭局", fromUserJsonObject.optString(TABLE_USER.NICKNAME.name()) + "邀请你加入饭局", pendingIntent);
		mNotificationManager.notify(0, notification);
	}

	private void showNotificationConfirm() {
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(getIntent().getStringExtra("message"));
			showDialog(this, jsonObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}

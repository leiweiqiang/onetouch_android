package com.tcl.onetouch;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.tcl.onetouch.api.RestaurantSearchByCategoryModel;
import com.tcl.onetouch.api.RideoJsonArrayRequest.RideoJsonArrayRequestListener;
import com.tcl.onetouch.event.RestaurantSelectEvent;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.JsonArrayBaseAdapter;
import com.tcl.onetouch.view.RecommendCellView;

import de.greenrobot.event.EventBus;

public class RestaurantSearchActivity extends IMServiceBaseActivity {

	private ListView listView;
	private ArrayList<JSONObject> selectArray;
	private SearchResultAdapter adapter;
	private ProgressDialog pd;
	private SearchView searchView;
	private ChatInfoClass chatInfoClass;

	private void hideSoftInput() {
		InputMethodManager inputMethodManager;
		inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		if (inputMethodManager != null) {
			View v = RestaurantSearchActivity.this.getCurrentFocus();
			if (v == null) {
				return;
			}

			inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
		inputMethodManager.hideSoftInputFromWindow(searchView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		searchView.clearFocus();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restaurant_search);
		findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		String chatInfoString = getIntent().getStringExtra("chatInfoClass");
		chatInfoClass = new ChatInfoClass(chatInfoString);

		searchView = (SearchView) findViewById(R.id.searchView1);

		String searchKey = getIntent().getStringExtra("searchKey");

		SearchView searchView = (SearchView) findViewById(R.id.searchView1);
		int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
		TextView textView = (TextView) searchView.findViewById(id);
		textView.setTextColor(Color.WHITE);
		searchView.setFocusable(false);

		searchView.setQuery(searchKey, true);

		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String text) {
				fetchRestaurantByCategory(text);
				hideSoftInput();
				return false;
			}

			@Override
			public boolean onQueryTextChange(String arg0) {
				// TODO Auto-generated method stub
				return false;
			}
		});

		selectArray = new ArrayList<JSONObject>();

		listView = (ListView) findViewById(R.id.listview);

		adapter = new SearchResultAdapter();
		listView.setAdapter(adapter);

		fetchRestaurantByCategory(searchKey);

		findViewById(R.id.dating_layout).setVisibility(View.INVISIBLE);

		// hideSoftInput();
	}

	private void addSelectRestaurant(RestaurantSelectEvent event) {
		selectArray.add(event.getJsonObject());
	}

	private void removeSelectRestaurant(RestaurantSelectEvent event) throws JSONException {
		for (int i = 0; i < selectArray.size(); i++) {
			JSONObject object = selectArray.get(i);
			if (object.optInt("id") == event.getJsonObject().optInt("id")) {
				selectArray.remove(i);
				break;
			}
		}
	}

	public void onRestsurantSelectEvent(RestaurantSelectEvent event) {
		if (event.isSelect()) {
			addSelectRestaurant(event);
		} else {
			try {
				removeSelectRestaurant(event);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		findViewById(R.id.dating_layout).setVisibility(selectArray.size() == 0 ? View.INVISIBLE : View.VISIBLE);

		TextView textView = (TextView) findViewById(R.id.sel_count);
		textView.setText("" + selectArray.size());
	}

	@Override
	protected void onStart() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onRestsurantSelectEvent", RestaurantSelectEvent.class);
		}
		super.onStart();
	}

	@Override
	protected void onStop() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onStop();
	}

	private void fetchRestaurantByCategory(String text) {
		pd = ProgressDialog.show(RestaurantSearchActivity.this, "���ڲ�ѯ", "���Ժ�");
		adapter.removeAll();

		RestaurantSearchByCategoryModel model = new RestaurantSearchByCategoryModel(text, new RideoJsonArrayRequestListener() {

			@Override
			public void onDataRecv(JSONArray jsonArray) {
				pd.dismiss();
				adapter.addArray(jsonArray);
				adapter.notifyDataSetChanged();
			}

			@Override
			public void onDataError(VolleyError error) {
				Toast.makeText(RestaurantSearchActivity.this, "��ѯʧ��", Toast.LENGTH_SHORT).show();
				pd.dismiss();
			}
		});
		model.fetch();
	}

	protected class SearchResultAdapter extends JsonArrayBaseAdapter {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				ViewHolder viewHolder = new ViewHolder(RestaurantSearchActivity.this);
				convertView = viewHolder.getTopItem();
				convertView.setTag(viewHolder);
			}
			ViewHolder holder = (ViewHolder) convertView.getTag();
			JSONObject object = array.optJSONObject(position);
			holder.setJsonData(object, position, isObjectSelect(object), chatInfoClass);
			return convertView;
		}
	}

	private static class ViewHolder {
		public RecommendCellView cellView;

		public ViewHolder(Context context) {
			cellView = new RecommendCellView(context);
		}

		public void setJsonData(JSONObject jsonObject, int position, boolean isSelect, ChatInfoClass chatInfoClass) {
			cellView.setJsonData(jsonObject, isSelect, chatInfoClass);
		}

		public RecommendCellView getTopItem() {
			return cellView;
		}
	}

	private boolean isObjectSelect(JSONObject object) {
		for (JSONObject jsonObject : selectArray) {
			if (jsonObject.optInt("id") == object.optInt("id")) {
				return true;
			}
		}
		return false;
	}

	public void onDatingBarClick(View view) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < selectArray.size(); i++) {
			jsonArray.put(selectArray.get(i));
		}

		if (jsonArray.length() > 0) {
			chatInfoClass.setSelectedRestaurants(jsonArray);
			imService.sendRestaurantMsg(chatInfoClass);
		}

		finish();
	}

	@Override
	protected void onIMServiceConnected() {
		// TODO Auto-generated method stub
		
	}

}

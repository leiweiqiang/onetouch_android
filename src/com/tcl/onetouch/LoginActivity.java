package com.tcl.onetouch;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tcl.onetouch.utils.UserData;
import com.tcl.onetouch.utils.UserData.USERDATA_FIELD;

public class LoginActivity extends IMServiceBaseActivity {

	private final String ACTION_XMPP_LOGIN_SUCCESS = "ACTION_XMPP_LOGIN_SUCCESS";
	private final String ACTION_XMPP_LOGIN_FAILED = "ACTION_XMPP_LOGIN_FAILED";

	private ProgressDialog pd;

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_XMPP_LOGIN_SUCCESS)) {
				if (pd != null) {
					pd.dismiss();
					pd = null;
				}
				redirectToMainActivity();
			} else if (action.equals(ACTION_XMPP_LOGIN_FAILED)) {
				if (pd != null) {
					pd.dismiss();
					pd = null;
				}
				Toast.makeText(getApplicationContext(), "��½ʧ��", Toast.LENGTH_SHORT).show();
			}
		}

	};

	@Override
	public void finish() {
		unregisterReceiver(mBroadcastReceiver);
		super.finish();
	}

	private String getPassword() {
		EditText editText = (EditText) findViewById(R.id.password);
		String password = editText.getText().toString();
		return password;
	}

	private String getUserName() {
		EditText editText = (EditText) findViewById(R.id.username);
		String username = editText.getText().toString();
		return username;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		Button login_button = (Button) findViewById(R.id.login_button);
		login_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (pd == null) {
					pd = ProgressDialog.show(LoginActivity.this, "���ڵ�½", "���Ժ�");
					imService.connect2xmpp(getUserName(), getPassword());
				}
			}
		});

		String username = UserData.getInstance(LoginActivity.this).getStringData(USERDATA_FIELD.USERNAME);
		String password = UserData.getInstance(LoginActivity.this).getStringData(USERDATA_FIELD.PASSWORD);

		if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
			EditText userEditText = (EditText) findViewById(R.id.username);
			userEditText.setText(username);

			EditText passwordEditText = (EditText) findViewById(R.id.password);
			passwordEditText.setText(password);
		}

		registerBoradcastReceiver();
		
	}

	@Override
	protected void onIMServiceConnected() {
		if (isIntentFromNotification()) {
			pd = ProgressDialog.show(LoginActivity.this, "���ڵ�½", "���Ժ�");
			imService.connect2xmpp(getUserName(), getPassword());
		}
		else {
			if (imService.isConnected()) {
				redirectToMainActivity();
			}
		}
	}

	protected void redirectToMainActivity() {
		Intent intent = new Intent(LoginActivity.this, MainActivity.class);
		if (isIntentFromNotification()) {
			intent.putExtra("message", getIntent().getStringExtra("message"));
			intent.putExtra(IS_SHOW_ALERT_FLAG, true);
		}
		startActivity(intent);
		finish();
	}

	public void registerBoradcastReceiver() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(ACTION_XMPP_LOGIN_SUCCESS);
		myIntentFilter.addAction(ACTION_XMPP_LOGIN_FAILED);
		registerReceiver(mBroadcastReceiver, myIntentFilter);
	}

}

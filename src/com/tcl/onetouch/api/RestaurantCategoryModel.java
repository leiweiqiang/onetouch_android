package com.tcl.onetouch.api;

public class RestaurantCategoryModel extends RideoJsonArrayRequest {

	public RestaurantCategoryModel(RideoJsonArrayRequestListener listener) {
		super(listener);
	}

	@Override
	protected String getRequestUrl() {
		RequestUrlBuilder requestUrlBuilder = new RequestUrlBuilder();
		requestUrlBuilder.appendUrl("/categories");
		return requestUrlBuilder.getUrlString();
	}
}

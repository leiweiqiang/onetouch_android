package com.tcl.onetouch.api;

import java.net.URLEncoder;

import com.tcl.onetouch.utils.GpsProvider;

import android.location.Location;

public class RestaurantRecommendModel extends RideoJsonRequest {

	private String text;

	public RestaurantRecommendModel(String text, CollectionListener listener) {
		super(listener);
		this.text = text;
	}

	@Override
	protected String getRequestUrl() {
		RequestUrlBuilder requestUrlBuilder = new RequestUrlBuilder();
		requestUrlBuilder.appendUrl("/search/text");
		requestUrlBuilder.addParam("t", URLEncoder.encode(text));
		Location location = GpsProvider.getCurrentLocation();
		if (location != null) {
			requestUrlBuilder.addParam("latitude", "" + location.getLatitude());
			requestUrlBuilder.addParam("longitude", "" + location.getLongitude());
		}
		String url = requestUrlBuilder.getUrlString();
		return url;
	}
}

package com.tcl.onetouch.api;


public class ContactModel extends RideoJsonArrayRequest {

	public ContactModel(RideoJsonArrayRequestListener listener) {
		super(listener);
	}

	@Override
	protected String getRequestUrl() {
		RequestUrlBuilder requestUrlBuilder = new RequestUrlBuilder();
		requestUrlBuilder.appendUrl("/contacts");
		requestUrlBuilder.addParam("user", "0");
		return requestUrlBuilder.getUrlString();
	}
}

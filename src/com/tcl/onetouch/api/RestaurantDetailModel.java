package com.tcl.onetouch.api;


public class RestaurantDetailModel extends RideoJsonArrayRequest {

	private String id;

	public RestaurantDetailModel(String id, RideoJsonArrayRequestListener listener) {
		super(listener);
		this.id = id;
	}

	@Override
	protected String getRequestUrl() {
		RequestUrlBuilder requestUrlBuilder = new RequestUrlBuilder();
		requestUrlBuilder.appendUrl("/restaurants");
		requestUrlBuilder.addParam("ids", id);
		return requestUrlBuilder.getUrlString();
	}
}

package com.tcl.onetouch.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tcl.onetouch.utils.Constants;

public class RequestUrlBuilder {

	private StringBuilder urlBuilder;
	private JSONArray paramArray;

	public RequestUrlBuilder() {
		super();
		urlBuilder = new StringBuilder();
		paramArray = new JSONArray();
	}

	public void appendUrl(String url) {
		if (urlBuilder.length() == 0) {
			urlBuilder.append(Constants.APP_URL_PREF);
		}
		urlBuilder.append(url);
	}

	public void addParam(String key, String value) {
		JSONObject object = new JSONObject();
		try {
			object.put(key, value);
			paramArray.put(object);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getUrlString() {
		for (int i = 0; i < paramArray.length(); i++) {
			urlBuilder.append(i == 0 ? "?" : "&");
			try {
				JSONObject object = paramArray.getJSONObject(i);
				JSONArray nameArray = object.names();
				if (nameArray != null && nameArray.length() > 0) {
					String name = nameArray.optString(0);
					urlBuilder.append(name);
					urlBuilder.append("=");
					urlBuilder.append(object.optString(name));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return urlBuilder.toString();
	}
}

package com.tcl.onetouch.api;

import org.json.JSONObject;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.tcl.onetouch.utils.VolleyRequestModule;

public abstract class RideoJsonRequest implements Listener<JSONObject>, ErrorListener {

	public interface CollectionListener {
		public void onDataRecv(JSONObject jsonObject);

		public void onDataError(VolleyError error);
	}

	protected CollectionListener listener;

	abstract protected String getRequestUrl();

	public RideoJsonRequest(CollectionListener listener) {
		super();
		this.listener = listener;
	}

	public void fetch() {
		String requestUrl = getRequestUrl();
		Log.v("", requestUrl);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestUrl, null, this, this);
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		VolleyRequestModule.getRequestQueue().add(jsonObjectRequest);
	}

	public void loadMore() {
		fetch();
	}

	public void refresh() {
		fetch();
	}

	@Override
	public void onResponse(JSONObject jsonObject) {
		if (listener != null) {
			listener.onDataRecv(jsonObject);
		}
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		if (listener != null) {
			listener.onDataError(error);
		}
	}
}

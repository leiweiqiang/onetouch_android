package com.tcl.onetouch.api;

import java.net.URLEncoder;

public class RestaurantSearchByCategoryModel extends RideoJsonArrayRequest {

	private String category;
	
	public RestaurantSearchByCategoryModel(String category, RideoJsonArrayRequestListener listener) {
		super(listener);
		this.category = category;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected String getRequestUrl() {
		RequestUrlBuilder requestUrlBuilder = new RequestUrlBuilder();
		requestUrlBuilder.appendUrl("/search/category");
		requestUrlBuilder.addParam("c", URLEncoder.encode(category));
		return requestUrlBuilder.getUrlString();
	}
}

package com.tcl.onetouch.api;

import java.net.URLEncoder;

public class ChatPOIModel extends RideoJsonArrayRequest {

	private String text;

	public ChatPOIModel(String text, RideoJsonArrayRequestListener listener) {
		super(listener);
		this.text = text;
	}

	@Override
	protected String getRequestUrl() {
		RequestUrlBuilder requestUrlBuilder = new RequestUrlBuilder();
		requestUrlBuilder.appendUrl("/poi/detect");
		requestUrlBuilder.addParam("text", URLEncoder.encode(text));
		return requestUrlBuilder.getUrlString();
	}
}

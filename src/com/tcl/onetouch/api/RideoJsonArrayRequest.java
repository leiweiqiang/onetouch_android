package com.tcl.onetouch.api;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.tcl.onetouch.utils.VolleyRequestModule;

public abstract class RideoJsonArrayRequest implements Listener<JSONArray>, ErrorListener {

	public interface RideoJsonArrayRequestListener {
		public void onDataRecv(JSONArray jsonArray);

		public void onDataError(VolleyError error);
	}

	protected RideoJsonArrayRequestListener listener;

	abstract protected String getRequestUrl();

	public RideoJsonArrayRequest(RideoJsonArrayRequestListener listener) {
		super();
		this.listener = listener;
	}

	public void fetch() {
		String requestUrl = getRequestUrl();
		Log.v("", requestUrl);
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(requestUrl, this, this);
		jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		VolleyRequestModule.getRequestQueue().add(jsonArrayRequest);
	}

	public void loadMore() {
		fetch();
	}

	public void refresh() {
		fetch();
	}

	@Override
	public void onResponse(JSONArray jsonArray) {
		if (listener != null) {
			listener.onDataRecv(jsonArray);
		}
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		if (listener != null) {
			listener.onDataError(error);
		}
	}
}

package com.tcl.onetouch;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.tcl.onetouch.api.RestaurantRecommendModel;
import com.tcl.onetouch.api.RestaurantSearchByCategoryModel;
import com.tcl.onetouch.api.RideoJsonArrayRequest.RideoJsonArrayRequestListener;
import com.tcl.onetouch.api.RideoJsonRequest.CollectionListener;
import com.tcl.onetouch.event.RestaurantSelectEvent;
import com.tcl.onetouch.event.SettingCategoryEvent;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.JsonArrayBaseAdapter;
import com.tcl.onetouch.utils.UserData;
import com.tcl.onetouch.utils.UserEntity;
import com.tcl.onetouch.view.ChatInfoView;
import com.tcl.onetouch.view.RecommendCellView;

import de.greenrobot.event.EventBus;

public class ChatPrepareActivity extends IMServiceBaseActivity {

	private ChatInfoClass chatInfoClass;
	private ChatInfoView chatInfoView;
	private ProgressDialog pd;
	private ListView listView;
	private RecommendAdapter adapter;

	private ArrayList<JSONObject> selectArray;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat_prepare);
		selectArray = new ArrayList<JSONObject>();

		chatInfoView = (ChatInfoView) findViewById(R.id.chat_info_view);

		listView = (ListView) findViewById(R.id.listview);
		adapter = new RecommendAdapter();
		listView.setAdapter(adapter);

		getRecommendRestaurant(getIntent().getStringExtra("keywords"));
	}

	@Override
	protected void onStart() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onSettingCategoryEvent", SettingCategoryEvent.class);
			EventBus.getDefault().register(this, "onRestsurantSelectEvent", RestaurantSelectEvent.class);
		}
		super.onStart();
	}

	@Override
	public void finish() {
		super.finish();
	}

	@Override
	protected void onStop() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onStop();
	}

	private void getRecommendRestaurant(String text) {

		pd = ProgressDialog.show(this, "���ڲ�ѯ", "���Ժ�");

		RestaurantRecommendModel model = new RestaurantRecommendModel(text, new CollectionListener() {

			@Override
			public void onDataRecv(JSONObject jsonObject) {
				setWaittingLayoutVisiable(false);
				chatInfoClass = new ChatInfoClass(jsonObject.toString());
				chatInfoClass.setSendFrom(new UserEntity(UserData.getInstance(ChatPrepareActivity.this).getFromUser()));
				chatInfoClass.setSendTo(new UserEntity(UserData.getInstance(ChatPrepareActivity.this).getToUser(jsonObject.optJSONObject("friend"))));
				chatInfoView.setChatInfoClass(chatInfoClass);
				adapter.addArray(jsonObject.optJSONArray("restaurants"));
				adapter.notifyDataSetChanged();
				pd.dismiss();
			}

			@Override
			public void onDataError(VolleyError error) {
				Toast.makeText(getApplicationContext(), "��ѯʧ��", Toast.LENGTH_SHORT).show();
				pd.dismiss();
			}
		});

		model.fetch();
	}

	private void fetchRestaurantByCategory(String text) {
		pd = ProgressDialog.show(this, "���ڲ�ѯ", "���Ժ�");
		adapter.removeAll();

		RestaurantSearchByCategoryModel model = new RestaurantSearchByCategoryModel(text, new RideoJsonArrayRequestListener() {

			@Override
			public void onDataRecv(JSONArray jsonArray) {
				pd.dismiss();
				adapter.addArray(jsonArray);
				adapter.notifyDataSetChanged();
			}

			@Override
			public void onDataError(VolleyError error) {
				Toast.makeText(ChatPrepareActivity.this, "��ѯʧ��", Toast.LENGTH_SHORT).show();
				pd.dismiss();
			}
		});
		model.fetch();
	}

	private void setWaittingLayoutVisiable(boolean isVisible) {
		findViewById(R.id.waitting_layout).setVisibility(isVisible ? View.VISIBLE : View.GONE);
	}

	private boolean isObjectSelect(JSONObject object) {
		for (JSONObject jsonObject : selectArray) {
			if (jsonObject.optInt("id") == object.optInt("id")) {
				return true;
			}
		}
		return false;
	}

	protected class RecommendAdapter extends JsonArrayBaseAdapter {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				ViewHolder viewHolder = new ViewHolder(ChatPrepareActivity.this);
				convertView = viewHolder.getTopItem();
				convertView.setTag(viewHolder);
			}
			ViewHolder holder = (ViewHolder) convertView.getTag();
			JSONObject object = array.optJSONObject(position);
			holder.setJsonData(object, position, isObjectSelect(object), chatInfoClass);
			return convertView;
		}
	}

	private static class ViewHolder {
		public RecommendCellView cellView;

		public ViewHolder(Context context) {
			cellView = new RecommendCellView(context);
		}

		public void setJsonData(JSONObject jsonObject, int position, boolean isSelect, ChatInfoClass chatInfoClass) {
			cellView.setJsonData(jsonObject, isSelect, chatInfoClass);
		}

		public RecommendCellView getTopItem() {
			return cellView;
		}
	}

	private void addSelectRestaurant(RestaurantSelectEvent event) {
		selectArray.add(event.getJsonObject());
	}

	private void removeSelectRestaurant(RestaurantSelectEvent event) throws JSONException {
		for (int i = 0; i < selectArray.size(); i++) {
			JSONObject object = selectArray.get(i);
			if (object.optInt("id") == event.getJsonObject().optInt("id")) {
				selectArray.remove(i);
				break;
			}
		}
	}

	public void onSettingCategoryEvent(SettingCategoryEvent event) {
		fetchRestaurantByCategory(event.getText());
	}

	public void onRestsurantSelectEvent(RestaurantSelectEvent event) {
		if (event.isSelect()) {
			addSelectRestaurant(event);
		} else {
			try {
				removeSelectRestaurant(event);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		TextView textView = (TextView) findViewById(R.id.sel_count);
		textView.setText("" + selectArray.size());
	}

	public void onDatingBarClick(View view) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < selectArray.size(); i++) {
			jsonArray.put(selectArray.get(i));
		}
		chatInfoClass.setSelectedRestaurants(jsonArray);

		if (selectArray.size() > 0) {
			imService.sendRestaurantMsg(chatInfoClass);
		}

		Intent intent = new Intent(this, IMActivity.class);
		intent.putExtra("chatInfoClass", chatInfoClass.toJsonObject().toString());
		startActivity(intent);
	}

	@Override
	protected void onIMServiceConnected() {
		
	}
}

package com.tcl.onetouch;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.tcl.onetouch.api.RestaurantDetailModel;
import com.tcl.onetouch.api.RideoJsonArrayRequest.RideoJsonArrayRequestListener;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.view.RestaurantDetailInfoView;

public class RestaurantDetailActivity extends Activity {

	private ChatInfoClass chatInfoClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restaurant_detail);

		String chatInfoString = getIntent().getStringExtra("chatInfoClass");
		chatInfoClass = new ChatInfoClass(chatInfoString);

		findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		String id = getIntent().getStringExtra("id");

		RestaurantDetailModel restaurantDetailModel = new RestaurantDetailModel(id, new RideoJsonArrayRequestListener() {

			@Override
			public void onDataError(VolleyError error) {

			}

			@Override
			public void onDataRecv(JSONArray jsonArray) {
				if (jsonArray.length() > 0) {
					JSONObject object = jsonArray.optJSONObject(0);
					setJsonObject(object);
				}
			}
		});

		restaurantDetailModel.fetch();
	}

	private void setJsonObject(JSONObject jsonObject) {
		setRestaurantData(jsonObject);
	}

	private void setRestaurantData(JSONObject object) {
		RestaurantDetailInfoView infoView = new RestaurantDetailInfoView(this);
		infoView.setJsonData(object, chatInfoClass);
		infoView.setOnClickListener(null);
		LinearLayout restaurant_info = (LinearLayout) findViewById(R.id.restaurant_info);
		restaurant_info.addView(infoView);

		TextView addressTextView = (TextView) findViewById(R.id.address);
		addressTextView.setText(object.optString("address"));

		TextView popularDishesTextView = (TextView) findViewById(R.id.popularDishes);
		StringBuilder stringBuilder = new StringBuilder();
		JSONArray array = object.optJSONArray("popularDishes");
		if (array != null) {
			findViewById(R.id.tuijian_layout).setVisibility(View.VISIBLE);
			for (int i = 0; i < array.length(); i++) {
				stringBuilder.append(array.optString(i) + "  ");
			}
			popularDishesTextView.setText(stringBuilder.toString());
		}

		TextView phoneTextView = (TextView) findViewById(R.id.phone);
		phoneTextView.setText(object.optString("telephone"));

		String dealString = object.optString("deal");
		TextView dianpingTextView = (TextView) findViewById(R.id.dianping);
		dianpingTextView.setText(dealString);
		findViewById(R.id.tuangou_layout).setVisibility(TextUtils.isEmpty(dealString) ? View.GONE : View.VISIBLE);

	}

}

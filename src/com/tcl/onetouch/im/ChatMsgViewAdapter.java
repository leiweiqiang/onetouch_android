package com.tcl.onetouch.im;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.event.BookedEvent;
import com.tcl.onetouch.event.ChatInfoEvent;
import com.tcl.onetouch.event.ChatPOIEvent;
import com.tcl.onetouch.provider.TABLE_MESSAGE;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.ImageDisplay;
import com.tcl.onetouch.utils.UserData;
import com.tcl.onetouch.utils.UserData.USERDATA_FIELD;
import com.tcl.onetouch.view.MsgRestTitleView;
import com.tcl.onetouch.view.ShareRestaurantView;

import de.greenrobot.event.EventBus;

public class ChatMsgViewAdapter extends BaseAdapter {

	private Cursor cursor;
	private ChatInfoClass chatInfoClass;

	public static interface IMsgViewType {
		int IMVT_COM_MSG = 0;
		int IMVT_TO_MSG = 1;
	}

	// private static final String TAG =
	// ChatMsgViewAdapter.class.getSimpleName();
	//
	private Context context;

	private LayoutInflater mInflater;

	public ChatMsgViewAdapter(Context context, ChatInfoClass chatInfoClass) {
		this.context = context;
		this.chatInfoClass = chatInfoClass;
		mInflater = LayoutInflater.from(context);
		updateCursor();
	}

	private void updateCursor() {
		String user = UserData.getInstance(context).getStringData(USERDATA_FIELD.USERNAME);

		cursor = context.getContentResolver().query(
				TABLE_MESSAGE.CONTENT_URI,
				null,
				"(" + TABLE_MESSAGE.USER.name() + "=? and " + TABLE_MESSAGE.MSG_TO.name() + "=? and " + TABLE_MESSAGE.MSG_FROM.name() + "=?) or (" + TABLE_MESSAGE.USER.name() + "=? and "
						+ TABLE_MESSAGE.MSG_TO.name() + "=? and " + TABLE_MESSAGE.MSG_FROM.name() + "=?)",
				new String[] { user, chatInfoClass.getSendTo().getName(), chatInfoClass.getSendFrom().getName(), user, chatInfoClass.getSendFrom().getName(), chatInfoClass.getSendTo().getName() },
				null);

	}

	@Override
	public void notifyDataSetChanged() {
		updateCursor();
		super.notifyDataSetChanged();
	}

	public int getCount() {
		int cnt = cursor.getCount();
		return cnt;
	}

	// private String getName(String name) {
	// int pos = name.lastIndexOf("@");
	// if (pos == -1) {
	// return "����Ա";
	// }
	// return name.substring(0, pos);
	// }

	public Object getItem(int position) {
		// ChatMsgEntity entity = new ChatMsgEntity();
		// entity.setFrom(cursor.getString(cursor.getColumnIndex(ChatMsgEntity.FROM)));
		// entity.setTo(cursor.getString(cursor.getColumnIndex(ChatMsgEntity.TO)));
		// long date =
		// cursor.getLong(cursor.getColumnIndex(ChatMsgEntity.DATE));
		// SimpleDateFormat newSimpleDateFormat = new
		// SimpleDateFormat(" MM-dd HH:mm:ss ", Locale.getDefault());
		// entity.setDate(newSimpleDateFormat.format(date));
		// entity.setText(cursor.getString(cursor.getColumnIndex(ChatMsgEntity.TEXT)));
		// entity.setSubject(cursor.getString(cursor.getColumnIndex(ChatMsgEntity.SUBJECT)));
		// String is_incomming_msg =
		// cursor.getString(cursor.getColumnIndex(ChatMsgEntity.IS_INCOMMING_MSG));
		// entity.setMsgType(is_incomming_msg.equalsIgnoreCase("true"));
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	private boolean isIncommingMessage() {
		String is_incomming_message = cursor.getString(TABLE_MESSAGE.IS_INCOMMING_MSG.ordinal());
		return is_incomming_message.equals("true");
	}

	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		cursor.moveToPosition(position);

		if (isIncommingMessage()) {
			return IMsgViewType.IMVT_COM_MSG;
		} else {
			return IMsgViewType.IMVT_TO_MSG;
		}

	}

	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	private String getDateString() {
		long time = cursor.getLong(TABLE_MESSAGE.DATE.ordinal());
		Date date = new Date(time);
		SimpleDateFormat newSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ", Locale.getDefault());
		return newSimpleDateFormat.format(date);
	}

	private View getImMessageView(int position, View convertView, ViewGroup parent) {
		boolean isComMsg = isIncommingMessage();

		ViewHolder viewHolder = null;
		// if (convertView == null) {
		if (isComMsg) {
			convertView = mInflater.inflate(R.layout.chatting_item_msg_text_left, null);
			ImageView iv_userhead = (ImageView) convertView.findViewById(R.id.iv_userhead);
			ImageDisplay.displayImage(chatInfoClass.getSendTo().getImage(), iv_userhead);

		} else {
			convertView = mInflater.inflate(R.layout.chatting_item_msg_text_right, null);
			ImageView iv_userhead = (ImageView) convertView.findViewById(R.id.iv_userhead);
			ImageDisplay.displayImage(chatInfoClass.getSendFrom().getImage(), iv_userhead);
		}

		viewHolder = new ViewHolder();
		viewHolder.tvSendTime = (TextView) convertView.findViewById(R.id.tv_sendtime);
		viewHolder.tvUserName = (TextView) convertView.findViewById(R.id.tv_username);
		viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tv_chatcontent);
		viewHolder.isComMsg = isComMsg;

		convertView.setTag(viewHolder);
		// } else {
		// viewHolder = (ViewHolder) convertView.getTag();
		// }

		viewHolder.tvSendTime.setText(getDateString());
		if (!isComMsg) {
			viewHolder.tvUserName.setText(chatInfoClass.getSendFrom().getNickName());
		} else {
			viewHolder.tvUserName.setText(chatInfoClass.getSendTo().getNickName());
		}
		viewHolder.tvContent.setText(cursor.getString(TABLE_MESSAGE.MSG_CONTENT.ordinal()));

		EventBus.getDefault().post(new ChatPOIEvent(cursor.getString(TABLE_MESSAGE.MSG_CONTENT.ordinal())));

		return convertView;
	}

	private View getShareRestraurantView(int position, View convertView, ViewGroup parent) {
		boolean isComMsg = isIncommingMessage();
		if (isComMsg) {
			convertView = mInflater.inflate(R.layout.chatting_item_msg_text_left, null);
			ImageView iv_userhead = (ImageView) convertView.findViewById(R.id.iv_userhead);
			ImageDisplay.displayImage(chatInfoClass.getSendTo().getImage(), iv_userhead);
		} else {
			convertView = mInflater.inflate(R.layout.chatting_item_msg_text_right, null);
			ImageView iv_userhead = (ImageView) convertView.findViewById(R.id.iv_userhead);
			ImageDisplay.displayImage(chatInfoClass.getSendFrom().getImage(), iv_userhead);
		}

		convertView.findViewById(R.id.tv_chatcontent).setVisibility(View.GONE);
		LinearLayout shareRestraurantView = (LinearLayout) convertView.findViewById(R.id.tv_restaurant);
		shareRestraurantView.setVisibility(View.VISIBLE);

		try {
			JSONObject jsonObject = new JSONObject(cursor.getString(TABLE_MESSAGE.MSG_CONTENT.ordinal()));
			MsgRestTitleView msgRestTitleView = new MsgRestTitleView(context);
			ChatInfoClass chatInfoClass = new ChatInfoClass(jsonObject.toString());

			msgRestTitleView.setJsonData(chatInfoClass.toJsonObject());
			EventBus.getDefault().post(new ChatInfoEvent(chatInfoClass));
			shareRestraurantView.addView(msgRestTitleView);

			JSONArray jsonArray = chatInfoClass.getSelectedRestaurants();

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.optJSONObject(i);
				if (object != null) {
					LinearLayout divider = new LinearLayout(context);
					divider.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
					divider.setBackgroundColor(context.getResources().getColor(R.color.light_gray_text_color));
					shareRestraurantView.addView(divider);

					ShareRestaurantView view = new ShareRestaurantView(context);
					view.setBackgroundResource(i == (jsonArray.length() - 1) ? R.drawable.im_msg_restaurant_bottom_bg : R.drawable.im_msg_restaurant_middle_bg);
					view.setJsonData(object, chatInfoClass);
					shareRestraurantView.addView(view);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		((TextView) convertView.findViewById(R.id.tv_sendtime)).setText(getDateString());
		if (!isComMsg) {
			((TextView) convertView.findViewById(R.id.tv_username)).setText(chatInfoClass.getSendFrom().getNickName());
		} else {
			((TextView) convertView.findViewById(R.id.tv_username)).setText(chatInfoClass.getSendTo().getNickName());
		}

		return convertView;
	}

	private View getBookingView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.booking_item_view, null);
		((TextView) convertView.findViewById(R.id.tv_sendtime)).setText(getDateString());
		return convertView;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		cursor.moveToPosition(position);
		String subject = cursor.getString(TABLE_MESSAGE.SUBJECT.ordinal());
		if (subject.equalsIgnoreCase("restraunt share") || subject.equalsIgnoreCase("restaurant share")) {
			return getShareRestraurantView(position, convertView, parent);
		} else if (subject.equalsIgnoreCase("booking")) {
			EventBus.getDefault().post(new BookedEvent(null));
			return getBookingView(position, convertView, parent);
		} else {
			return getImMessageView(position, convertView, parent);
		}
	}

	static class ViewHolder {
		public TextView tvSendTime;
		public TextView tvUserName;
		public TextView tvContent;
		public boolean isComMsg = true;
	}

}

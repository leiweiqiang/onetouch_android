package com.tcl.onetouch.event;

public class ChatPOIEvent {
	
	private String text;
	
	public ChatPOIEvent(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

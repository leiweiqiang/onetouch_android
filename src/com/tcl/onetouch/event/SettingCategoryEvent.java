package com.tcl.onetouch.event;

public class SettingCategoryEvent {
	
	private String text;
	
	public SettingCategoryEvent(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

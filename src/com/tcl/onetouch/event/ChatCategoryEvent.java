package com.tcl.onetouch.event;

public class ChatCategoryEvent {
	
	private String text;
	
	public ChatCategoryEvent(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

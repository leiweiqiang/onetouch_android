package com.tcl.onetouch.event;

import org.json.JSONObject;

public class BookedEvent {
	
	private JSONObject jsonObject;
	
	public BookedEvent(JSONObject jsonObject) {
		super();
		this.jsonObject = jsonObject;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

}

package com.tcl.onetouch.event;

import org.json.JSONObject;

public class RestaurantSelectEvent {
	
	private JSONObject jsonObject;
	private boolean select;
	
	public RestaurantSelectEvent(JSONObject jsonObject, boolean select) {
		super();
		this.select = select;
		this.jsonObject = jsonObject;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}

	

}

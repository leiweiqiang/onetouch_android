package com.tcl.onetouch.event;

import org.json.JSONObject;

public class ChatUserEvent {
	
	private JSONObject jsonObject;
	
	public ChatUserEvent(JSONObject jsonObject) {
		super();
		this.jsonObject = jsonObject;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

}

package com.tcl.onetouch.event;

public class LocationSearchEvent {
	
	private String text;
	
	public LocationSearchEvent(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

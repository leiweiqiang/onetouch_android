package com.tcl.onetouch.event;

import java.util.Date;

public class ChatDateEvent {
	
	private Date date;
	
	public ChatDateEvent(Date date) {
		super();
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


}

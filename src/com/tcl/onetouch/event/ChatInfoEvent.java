package com.tcl.onetouch.event;

import com.tcl.onetouch.utils.ChatInfoClass;

public class ChatInfoEvent {
	
	private ChatInfoClass chatInfoClass;
	
	public ChatInfoEvent(ChatInfoClass chatInfoClass) {
		super();
		this.chatInfoClass = chatInfoClass;
	}

	public ChatInfoClass getChatInfoClass() {
		return chatInfoClass;
	}

	public void setChatInfoClass(ChatInfoClass chatInfoClass) {
		this.chatInfoClass = chatInfoClass;
	}


}

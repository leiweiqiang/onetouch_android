package com.tcl.onetouch.event;

import org.json.JSONArray;

public class ContactRefreshEvent {

	private JSONArray jsonArray;

	public ContactRefreshEvent(JSONArray jsonArray) {
		super();
		this.jsonArray = jsonArray;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

}

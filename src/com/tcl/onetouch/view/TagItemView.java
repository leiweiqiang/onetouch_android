package com.tcl.onetouch.view;

import org.jivesoftware.smackx.carbons.Carbon.Private;
import org.xbill.DNS.tests.primary;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.RestaurantSearchActivity;
import com.tcl.onetouch.utils.ChatInfoClass;

public class TagItemView extends TextView {
	private ChatInfoClass chatInfoClass;

	public TagItemView(Context context, ChatInfoClass chatInfoClass) {
		super(context);
		this.chatInfoClass = chatInfoClass;
		setTextColor(Color.WHITE);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		params.setMargins(0, 0, 24, 0);
		setPadding(24, 0, 24, 0);
		setGravity(Gravity.CENTER);
		setLayoutParams(params);
		setBackgroundDrawable(context.getResources().getDrawable(R.drawable.tag_background));

		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				openSearchRestaurantActivity();
			}
		});
		

//		setOnTouchListener(new OnTouchListener() {
//
//			@Override
//			public boolean onTouch(View view, MotionEvent event) {
//				switch (event.getAction()) {
//				case MotionEvent.ACTION_DOWN:
//					setTextColor(getResources().getColor(R.color.second_color));
//					break;
//
//				case MotionEvent.ACTION_UP:
//				case MotionEvent.ACTION_MOVE:
//					setTextColor(Color.WHITE);
//					break;
//
//				default:
//					break;
//				}
//				return false;
//			}
//		});
	}

	private void openSearchRestaurantActivity() {
		Intent intent = new Intent(getContext(), RestaurantSearchActivity.class);
		intent.putExtra("searchKey", getText());
		intent.putExtra("chatInfoClass", chatInfoClass.toJsonObject().toString());
		getContext().startActivity(intent);
	}
}

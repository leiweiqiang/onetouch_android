package com.tcl.onetouch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.event.ChatCategoryEvent;
import com.tcl.onetouch.event.ChatDateEvent;
import com.tcl.onetouch.event.ChatUserEvent;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.UserEntity;

import de.greenrobot.event.EventBus;

public class ChatInfoView extends LinearLayout {

	private ChatInfoClass chatInfoClass;

	private boolean isIMActivity = false;

	public ChatInfoView(Context context) {
		super(context);
		init(context);
	}

	public ChatInfoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.chat_info_view, this);
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		findViewById(R.id.setting_time).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				View parentView = view.getRootView();
				RelativeLayout container = (RelativeLayout) parentView.findViewById(R.id.activity_chat_prepare);
				SettingTimeView settingTimeView = new SettingTimeView(getContext());
				settingTimeView.setCalendar(chatInfoClass.getCalendar());
				container.addView(settingTimeView);
			}
		});

		findViewById(R.id.setting_user).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				View parentView = view.getRootView();
				RelativeLayout container = (RelativeLayout) parentView.findViewById(R.id.activity_chat_prepare);
				SettingUserView settingUserView = new SettingUserView(getContext());
				container.addView(settingUserView);
			}
		});

		findViewById(R.id.setting_category).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				View parentView = view.getRootView();
				RelativeLayout container = (RelativeLayout) parentView.findViewById(R.id.activity_chat_prepare);
				SettingCategoryView settingCategoryView = new SettingCategoryView(getContext());
				settingCategoryView.setChatInfoClass(chatInfoClass);
				container.addView(settingCategoryView);
				if (isIMActivity) {
					settingCategoryView.setIsIMCategoryView();
				}
			}
		});

		findViewById(R.id.setting_location).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				View parentView = view.getRootView();
				RelativeLayout container = (RelativeLayout) parentView.findViewById(R.id.activity_chat_prepare);
				SettingLocationView settingLocationView = new SettingLocationView(getContext());
				container.addView(settingLocationView);
				settingLocationView.setLocationData(chatInfoClass.getLocation());
			}
		});

	}

	public ChatInfoClass getChatInfoClass() {
		return chatInfoClass;
	}

	public void setChatInfoClass(ChatInfoClass chatInfoClass) {
		this.chatInfoClass = chatInfoClass;
		setChatDate();
		setChatUser();
		setChatLocation();
		setChatCategory();
	}

	public void disableUserClick() {
		findViewById(R.id.setting_user).setOnClickListener(null);
	}

	public void onChatDateEvent(ChatDateEvent event) {
		if (chatInfoClass != null) {
			chatInfoClass.setTime(event.getDate());
			setChatDate();
		}
	}

	public void onChatUserEvent(ChatUserEvent event) {
		if (chatInfoClass != null) {
			chatInfoClass.setSendTo(new UserEntity(event.getJsonObject()));
			setChatUser();
		}
	}

	public void onChatCategoryEvent(ChatCategoryEvent event) {
		if (chatInfoClass != null) {
			chatInfoClass.setCategory(event.getText());
			setChatCategory();
		}
	}

	@Override
	protected void onAttachedToWindow() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onChatDateEvent", ChatDateEvent.class);
			EventBus.getDefault().register(this, "onChatUserEvent", ChatUserEvent.class);
			EventBus.getDefault().register(this, "onChatCategoryEvent", ChatCategoryEvent.class);
		}
		super.onAttachedToWindow();
	}

	@Override
	protected void onDetachedFromWindow() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onDetachedFromWindow();
	}

	private void setChatDate() {
		if (chatInfoClass != null) {
			TextView textView = (TextView) findViewById(R.id.chat_date);
			textView.setText(chatInfoClass.getTimeShortString());
		}
	}

	private void setChatUser() {
		if (chatInfoClass != null) {
			TextView textView = (TextView) findViewById(R.id.chat_user);
			textView.setText(chatInfoClass.getSendTo().getNickName());
		}
	}

	private void setChatLocation() {
		if (chatInfoClass != null) {
			TextView textView = (TextView) findViewById(R.id.chat_location);
			textView.setText(chatInfoClass.getLocationName());
		}
	}

	private void setChatCategory() {
		if (chatInfoClass != null) {
			TextView textView = (TextView) findViewById(R.id.chat_category);
			textView.setText(chatInfoClass.getCategory());
		}
	}

	public boolean isIMActivity() {
		return isIMActivity;
	}

	public void setIMActivity(boolean isIMActivity) {
		this.isIMActivity = isIMActivity;
	}
}

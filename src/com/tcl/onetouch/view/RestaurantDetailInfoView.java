package com.tcl.onetouch.view;

import org.json.JSONObject;

import com.tcl.onetouch.R;
import com.tcl.onetouch.utils.ChatInfoClass;

import android.content.Context;
import android.view.View;

public class RestaurantDetailInfoView extends RecommendCellView {

	public RestaurantDetailInfoView(Context context) {
		super(context);
		setOnClickListener(null);
	}
	
	public void setJsonData(JSONObject object, ChatInfoClass chatInfoClass) {
		setJsonData(object, false, chatInfoClass);
		findViewById(R.id.add_share).setVisibility(View.GONE);
	}

}

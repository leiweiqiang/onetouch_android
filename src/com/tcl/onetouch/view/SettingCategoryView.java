package com.tcl.onetouch.view;

import java.util.ArrayList;

import org.json.JSONArray;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.tcl.onetouch.R;
import com.tcl.onetouch.RestaurantSearchActivity;
import com.tcl.onetouch.api.RestaurantCategoryModel;
import com.tcl.onetouch.api.RideoJsonArrayRequest.RideoJsonArrayRequestListener;
import com.tcl.onetouch.datatimepicker.NumberPicker;
import com.tcl.onetouch.datatimepicker.NumberPicker.OnValueChangeListener;
import com.tcl.onetouch.event.ChatCategoryEvent;
import com.tcl.onetouch.event.SettingCategoryEvent;
import com.tcl.onetouch.utils.ChatInfoClass;

import de.greenrobot.event.EventBus;

public class SettingCategoryView extends LinearLayout implements OnClickListener {

	private NumberPicker categoryPicker;
	private ArrayList<String> arrayList = new ArrayList<String>();
	private ChatInfoClass chatInfoClass;

	public void setChatInfoClass(ChatInfoClass chatInfoClass) {
		this.chatInfoClass = chatInfoClass;
	}

	public SettingCategoryView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.setting_category_view, this);
		setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SettingCategoryView.this.setVisibility(View.GONE);
			}
		});

		findViewById(R.id.mic).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});

		categoryPicker = (NumberPicker) findViewById(R.id.category_picker);
		categoryPicker.setVisibility(View.INVISIBLE);
		getRestaurantCategories();

		categoryPicker.setOnValueChangedListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				setSelectId(newVal);
			}
		});

		findViewById(R.id.search).setOnClickListener(this);

		EditText search_edit = (EditText) findViewById(R.id.search_key);
		search_edit.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View view, boolean isFoucus) {
				if (isFoucus) {
					EditText search_edit = (EditText) view;
					search_edit.setText("");
				}

			}
		});

	}

	public void setIsIMCategoryView() {
		findViewById(R.id.search).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SettingCategoryView.this.setVisibility(View.GONE);
				EditText search_edit = (EditText) findViewById(R.id.search_key);
				Intent intent = new Intent(getContext(), RestaurantSearchActivity.class);
				intent.putExtra("searchKey", search_edit.getText().toString());
				intent.putExtra("chatInfoClass", chatInfoClass.toJsonObject().toString());
				getContext().startActivity(intent);
			}
		});
	}

	private void setSelectId(int id) {
		EditText search_edit = (EditText) findViewById(R.id.search_key);
		search_edit.setText(arrayList.get(id));
		EventBus.getDefault().post(new ChatCategoryEvent(arrayList.get(id)));
	}

	private void getRestaurantCategories() {
		RestaurantCategoryModel model = new RestaurantCategoryModel(new RideoJsonArrayRequestListener() {

			@Override
			public void onDataRecv(JSONArray jsonArray) {
				categoryPicker.setVisibility(View.VISIBLE);
				categoryPicker.setMinValue(0);
				categoryPicker.setMaxValue(jsonArray.length() - 1);
				categoryPicker.setWrapSelectorWheel(false);
				for (int i = 0; i < jsonArray.length(); i++) {
					arrayList.add(jsonArray.optString(i));
				}
				categoryPicker.setDisplayedValues((String[]) arrayList.toArray(new String[arrayList.size()]));
				setCategoryPickerSelectId(jsonArray);
			}

			@Override
			public void onDataError(VolleyError error) {

			}
		});

		model.fetch();
	}

	private void setCategoryPickerSelectId(JSONArray jsonArray) {
		int selectId = 0;
		for (int i = 0; i < jsonArray.length(); i++) {
			String name = jsonArray.optString(i);
			if (chatInfoClass.getCategory().equals(name)) {
				selectId = i;
			}
		}
		categoryPicker.setValue(selectId);
	}

	@Override
	public void onClick(View arg0) {
		SettingCategoryView.this.setVisibility(View.GONE);
		EditText search_edit = (EditText) findViewById(R.id.search_key);
		EventBus.getDefault().post(new SettingCategoryEvent(search_edit.getText().toString()));
	}
}

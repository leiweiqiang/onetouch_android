package com.tcl.onetouch.view;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tcl.onetouch.R;
import com.tcl.onetouch.utils.JsonArrayBaseAdapter;
import com.tcl.onetouch.utils.UserData;

public class SettingUserView extends LinearLayout {

	public SettingUserView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.setting_user_view, this);
		setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SettingUserView.this.setVisibility(View.GONE);
			}
		});

		findViewById(R.id.mic).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});

		findViewById(R.id.add_from_pb).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});

		GridView user_gridView = (GridView) findViewById(R.id.user_list);
		UserAdapter adapter = new UserAdapter();
		JSONArray adapterArray = UserData.getInstance(context).getUserArray();
		adapter.addArray(adapterArray);
		user_gridView.setAdapter(adapter);
	}

	protected class UserAdapter extends JsonArrayBaseAdapter {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				ViewHolder viewHolder = new ViewHolder(SettingUserView.this.getContext());
				convertView = viewHolder.getTopItem();
				convertView.setTag(viewHolder);
			}
			ViewHolder holder = (ViewHolder) convertView.getTag();
			JSONObject object = array.optJSONObject(position);
			holder.setJsonData(object);
			return convertView;
		}
	}

	private class ViewHolder {
		public UserCellView cellView;

		public ViewHolder(Context context) {
			cellView = new UserCellView(context);
		}

		public void setJsonData(JSONObject jsonObject) {
			cellView.setJsonData(jsonObject);
		}

		public UserCellView getTopItem() {
			return cellView;
		}
	}

}

package com.tcl.onetouch.view;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tcl.onetouch.R;
import com.tcl.onetouch.RestaurantDetailActivity;
import com.tcl.onetouch.utils.AddShareClass;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.ImageDisplay;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ShareRestaurantView extends LinearLayout {
	
	private JSONObject object;
	private ChatInfoClass chatInfoClass;

	public ShareRestaurantView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.im_share_restaurant_cell_view, this);
		setBackgroundColor(Color.WHITE);

		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				startRestaurantDetailActivity();
			}
		});
	}
	
	private void startRestaurantDetailActivity(){
		Intent intent = new Intent(getContext(), RestaurantDetailActivity.class);
		intent.putExtra("id", object.optString("id"));
		intent.putExtra("chatInfoClass", chatInfoClass.toJsonObject().toString());
		getContext().startActivity(intent);
	}

	public void setJsonData(JSONObject object,ChatInfoClass chatInfoClass ) {
		this.object = object;
		this.chatInfoClass = chatInfoClass;
		setName(object);
		setCategories(object);
		setImage(object);
		setDistance();
		// setAddShareClass(object);
	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		float[] results = new float[1];
		Location.distanceBetween(lat1, lon1, lat2, lon2, results);
		return results[0];
	}

	private void setDistance() {
		TextView textView = (TextView) findViewById(R.id.distance);
		double distance = getDistance(Double.parseDouble(object.optString("latitude")), Double.parseDouble(object.optString("longitude")),
				Double.parseDouble(chatInfoClass.getLocation().optString("latitude")), Double.parseDouble(chatInfoClass.getLocation().optString("longitude")));
		textView.setText(String.format("%.1f km", distance / 1000));
	}

	// private void setAddShareClass(JSONObject object) {
	// AddShareClass addShareClass = (AddShareClass)
	// findViewById(R.id.add_share);
	// addShareClass.setJsonObject(object, true);
	// }

	private void setName(JSONObject object) {
		TextView textView = (TextView) findViewById(R.id.name);
		textView.setText(object.optString("name"));
	}

	private void setCategories(JSONObject object) {
		TextView textView = (TextView) findViewById(R.id.categories);
		StringBuilder stringBuilder = new StringBuilder();
		JSONArray array = object.optJSONArray("categories");
		for (int i = 0; i < array.length(); i++) {
			try {
				stringBuilder.append(array.getString(i));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		textView.setText(stringBuilder.toString());
	}

	private void setImage(JSONObject object) {
		ImageView imageView = (ImageView) findViewById(R.id.image);
		ImageDisplay.cancelDisplayImage(imageView);
		ImageDisplay.displayMyTagImage(object.optString("image"), imageView);
	}

}

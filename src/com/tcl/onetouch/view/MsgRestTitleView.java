package com.tcl.onetouch.view;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tcl.onetouch.R;

public class MsgRestTitleView extends LinearLayout {

	public MsgRestTitleView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.msg_rest_title, this);
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	}
	
	public void setJsonData(JSONObject jsonObject){
		TextView timeTextView = (TextView) findViewById(R.id.time);
		timeTextView.setText("ʱ��: " + jsonObject.optString("time"));

		TextView locationTextView = (TextView) findViewById(R.id.location);
		JSONObject locationObject = jsonObject.optJSONObject("location");
		locationTextView.setText("�ص�: " + locationObject.optString("name"));

	}

}

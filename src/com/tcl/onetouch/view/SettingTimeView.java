package com.tcl.onetouch.view;

import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tcl.onetouch.R;
import com.tcl.onetouch.datatimepicker.DatePicker;
import com.tcl.onetouch.datatimepicker.DatePicker.OnDateChangeListener;
import com.tcl.onetouch.datatimepicker.TimePicker;
import com.tcl.onetouch.datatimepicker.TimePicker.OnTimeChangeListener;
import com.tcl.onetouch.event.ChatDateEvent;

import de.greenrobot.event.EventBus;

public class SettingTimeView extends LinearLayout {

	private DatePicker datePicker;
	private TimePicker timePicker;

	public SettingTimeView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.setting_time_view, this);
		setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				View parentView = view.getRootView();
				RelativeLayout container = (RelativeLayout) parentView.findViewById(R.id.activity_chat_prepare);
				container.removeView(SettingTimeView.this);
			}
		});

		findViewById(R.id.mic).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});

		datePicker = (DatePicker) findViewById(R.id.datePicker1);
		datePicker.setListener(new OnDateChangeListener() {

			@Override
			public void onValueChange(DatePicker picker, Date newVal) {
				EventBus.getDefault().post(new ChatDateEvent(newVal));
			}
		});

		timePicker = (TimePicker) findViewById(R.id.timePicker1);
		timePicker.setListener(new OnTimeChangeListener() {

			@Override
			public void onValueChange(TimePicker picker, Date newVal) {
				EventBus.getDefault().post(new ChatDateEvent(newVal));
			}
		});
	}
	
	public void setCalendar(Calendar calendar){
		datePicker.setCalendar(calendar);
		timePicker.setCalendar(calendar);
	}

}

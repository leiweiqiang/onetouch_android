package com.tcl.onetouch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tcl.onetouch.R;

public class RecommendTopButton extends LinearLayout {

	public RecommendTopButton(Context context) {
		super(context);
		init(context);
	}

	public RecommendTopButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.recommend_top_button, this);
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}
	
	public void setBtnTitle(String title) {
		TextView textView = (TextView) findViewById(R.id.btn_title);
		textView.setText(title);
	}
	
	public void setBtnValue(String value) {
		TextView textView = (TextView) findViewById(R.id.btn_value);
		textView.setText(value.isEmpty()?"--":value);
	}
}

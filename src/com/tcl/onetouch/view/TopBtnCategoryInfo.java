package com.tcl.onetouch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.event.ChatCategoryEvent;

import de.greenrobot.event.EventBus;

public class TopBtnCategoryInfo extends RecommendTopButton {

	@Override
	protected void onAttachedToWindow() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onChatCategoryEvent", ChatCategoryEvent.class);
		}
		super.onAttachedToWindow();
	}

	public TopBtnCategoryInfo(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public TopBtnCategoryInfo(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDetachedFromWindow() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onDetachedFromWindow();
	}

	public void onChatCategoryEvent(ChatCategoryEvent event) {
		setBtnValue(event.getText());
	}
	
	public String getCategory() {
		TextView textView = (TextView) findViewById(R.id.btn_value);
		return textView.getText().toString();
	}

}

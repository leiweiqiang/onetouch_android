package com.tcl.onetouch.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.util.AttributeSet;

import com.tcl.onetouch.event.ChatDateEvent;

import de.greenrobot.event.EventBus;

public class TopBtnDateInfo extends RecommendTopButton {

	private Date date = new Date();

	@Override
	protected void onAttachedToWindow() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onChatDateEvent", ChatDateEvent.class);
		}
		super.onAttachedToWindow();
	}

	public TopBtnDateInfo(Context context, AttributeSet attrs) {
		super(context, attrs);
		update();
	}

	public TopBtnDateInfo(Context context) {
		super(context);
	}

	@Override
	protected void onDetachedFromWindow() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onDetachedFromWindow();
	}

	public void onChatDateEvent(ChatDateEvent event) {
		date = event.getDate();
		update();
	}

	private void update() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm", Locale.getDefault());
		setBtnValue(sdf.format(date));
	}

	public Date getDate() {
		return date;
	}

	public Date setYYMMDD(Date date) {
		this.date.setYear(date.getYear());
		this.date.setMonth(date.getMonth());
		this.date.setDate(date.getDate());
		return this.date;
	}

	public Date setHHMMSS(Date date) {
		this.date.setHours(date.getHours());
		this.date.setMinutes(date.getMinutes());
		this.date.setSeconds(date.getSeconds());
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
		update();
	}

}

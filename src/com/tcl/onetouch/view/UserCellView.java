package com.tcl.onetouch.view;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.event.ChatUserEvent;
import com.tcl.onetouch.utils.ImageDisplay;
import com.tcl.onetouch.utils.UserEntity;

import de.greenrobot.event.EventBus;

public class UserCellView extends LinearLayout {

	private JSONObject jsonObject;

	public UserCellView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.user_cell_view, this);
		setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));

		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				EventBus.getDefault().post(new ChatUserEvent(jsonObject));
			}
		});
	}

	public void setJsonData(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
		UserEntity userEntity = new UserEntity(jsonObject);
		ImageView userImageView = (ImageView) findViewById(R.id.user_icon);
		ImageDisplay.displayCircleImage(userEntity.getImage(), userImageView);

		TextView usernameTextView = (TextView) findViewById(R.id.username);
		usernameTextView.setText(userEntity.getNickName());
	}
}

package com.tcl.onetouch.view;

import org.json.JSONObject;

import android.content.Context;
import android.util.AttributeSet;

import com.tcl.onetouch.event.ChatLocationEvent;

import de.greenrobot.event.EventBus;

public class TopBtnLocationInfo extends RecommendTopButton {

	private JSONObject location;

	public JSONObject getLocation() {
		return location;
	}

	public void setLocation(JSONObject location) {
		this.location = location;
	}

	@Override
	protected void onAttachedToWindow() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onChatLocationEvent", ChatLocationEvent.class);
		}
		super.onAttachedToWindow();
	}

	public TopBtnLocationInfo(Context context, AttributeSet attrs) {
		super(context, attrs);
//		update();
	}

	public TopBtnLocationInfo(Context context) {
		super(context);
	}

	@Override
	protected void onDetachedFromWindow() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onDetachedFromWindow();
	}

	public void onChatLocationEvent(ChatLocationEvent event) {
		location = event.getJsonObject();
		update();
	}

	private void update() {
		setBtnValue(location.optString("name"));
	}
	
	

}

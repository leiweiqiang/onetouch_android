package com.tcl.onetouch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.event.ChatCategoryEvent;
import com.tcl.onetouch.event.ChatDateEvent;
import com.tcl.onetouch.event.ChatUserEvent;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.UserEntity;

import de.greenrobot.event.EventBus;

public class ChatStateView extends LinearLayout {


	public ChatStateView(Context context) {
		super(context);
		init(context);
	}

	public ChatStateView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.chat_state_view, this);
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	}
}

package com.tcl.onetouch.view;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.navdrawer.SimpleSideDrawer;
import com.tcl.onetouch.LoginActivity;
import com.tcl.onetouch.R;
import com.tcl.onetouch.event.ContactRefreshEvent;
import com.tcl.onetouch.utils.ImageDisplay;
import com.tcl.onetouch.utils.UserData;
import com.tcl.onetouch.utils.UserData.USERDATA_FIELD;

import de.greenrobot.event.EventBus;

public class SideMenuView extends SimpleSideDrawer {

	private Activity activity;

	public SideMenuView(Activity act) {
		super(act);
		activity = act;
		setLeftBehindContentView(R.layout.side_menu_view);

		findViewById(R.id.logout_button).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				new AlertDialog.Builder(activity).setTitle("确定要退出登录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						UserData.getInstance(getContext()).setStringData(USERDATA_FIELD.USERNAME, "");
						UserData.getInstance(getContext()).setStringData(USERDATA_FIELD.PASSWORD, "");
//						XMPPConnection connection = XmppConnectionManager.getInstance().getConnection();
//						connection.disconnect();
						Intent intent = new Intent(activity, LoginActivity.class);
						activity.startActivity(intent);
						activity.finish();
					}
				}).setNeutralButton("取消", null).show();

			}
		});
	}
	
	private JSONObject getUserJsonObject(String username, JSONArray jsonArray){
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.optJSONObject(i);
			if (jsonObject.optString("username").equals(username)) {
				return jsonObject;
			}
		}
		return null;
	}



	@Override
	protected void onAttachedToWindow() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onContactRefreshEvent", ContactRefreshEvent.class);
		}
		super.onAttachedToWindow();
	}

	@Override
	protected void onDetachedFromWindow() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onDetachedFromWindow();
	}

	public void onContactRefreshEvent(ContactRefreshEvent event) {
		String username = UserData.getInstance(getContext()).getStringData(USERDATA_FIELD.USERNAME);
		JSONObject userJsonObject = getUserJsonObject(username, event.getJsonArray());

		ImageView userImageView = (ImageView) findViewById(R.id.user_image);
		ImageDisplay.displayImage(userJsonObject.optString("image"), userImageView);
//		userImageView.setImageResource(username.equalsIgnoreCase("111") ? R.drawable.xiaohei : R.drawable.renma);
		userImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				toggleLeftDrawer();
			}
		});

		TextView usernameTextView = (TextView) findViewById(R.id.username);
		usernameTextView.setText(userJsonObject.optString("nickname"));

	}
}

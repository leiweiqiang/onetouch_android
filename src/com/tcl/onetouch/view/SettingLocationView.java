package com.tcl.onetouch.view;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.tcl.onetouch.R;

public class SettingLocationView extends LinearLayout {

	public MapView mMapView = null;

	public SettingLocationView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.setting_location_view, this);

		mMapView = (MapView) findViewById(R.id.bmapView);

		setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SettingLocationView.this.setVisibility(View.GONE);
			}
		});

		findViewById(R.id.mic).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});

	}

	public void setLocationData(JSONObject object) {

		BaiduMap mBaiduMap = mMapView.getMap();
		LatLng latLng = new LatLng(object.optDouble("latitude"), object.optDouble("longitude"));
		MapStatusUpdate statusUpdate = MapStatusUpdateFactory.newLatLng(latLng);
		mBaiduMap.setMapStatus(statusUpdate);
		MapStatusUpdate ms = MapStatusUpdateFactory.zoomTo(17);
		mBaiduMap.setMapStatus(ms);

		OverlayOptions overlayOptions = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)).zIndex(5);
		Marker marker = (Marker) (mBaiduMap.addOverlay(overlayOptions));
		
		EditText editText = (EditText) findViewById(R.id.location_name);
		editText.setText(object.optString("name"));

	}

}

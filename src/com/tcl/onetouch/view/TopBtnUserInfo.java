package com.tcl.onetouch.view;

import android.content.Context;
import android.util.AttributeSet;

import com.tcl.onetouch.event.ChatUserEvent;

import de.greenrobot.event.EventBus;

public class TopBtnUserInfo extends RecommendTopButton {

	@Override
	protected void onAttachedToWindow() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this, "onChatUserEvent", ChatUserEvent.class);
		}
		super.onAttachedToWindow();
	}

	public TopBtnUserInfo(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public TopBtnUserInfo(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDetachedFromWindow() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onDetachedFromWindow();
	}

	public void onChatUserEvent(ChatUserEvent event) {
		setBtnValue(event.getJsonObject().optString("nickname"));
	}

}

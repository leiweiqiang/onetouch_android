package com.tcl.onetouch.view;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tcl.onetouch.R;
import com.tcl.onetouch.RestaurantDetailActivity;
import com.tcl.onetouch.utils.AddShareClass;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.ImageDisplay;

public class RecommendCellView extends LinearLayout {

	private AddShareClass addShareClass;
	private TextView nameTextView;
	private TextView categoriesTextView;
	private TextView dealTextView;

	private ImageView imageView;
	private JSONObject jsonObject;
	private ChatInfoClass chatInfoClass;

	public RecommendCellView(Context context) {
		super(context);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.recommend_cell_view, this);
		addShareClass = (AddShareClass) findViewById(R.id.add_share);
		nameTextView = (TextView) findViewById(R.id.name);
		categoriesTextView = (TextView) findViewById(R.id.categories);
		dealTextView = (TextView) findViewById(R.id.deal);
		imageView = (ImageView) findViewById(R.id.image);
		setBackgroundColor(Color.WHITE);
	}

	public void setJsonData(JSONObject object, boolean isSelect, ChatInfoClass chatInfoClass) {
		jsonObject = object;
		this.chatInfoClass = chatInfoClass;
		setName(object);
		setCategories(object);
		setImage(object);
		setDeal(object);
		setAddShareClass(object, isSelect);
		setDistance();
		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startRestaurantDetailActivity();
			}
		});
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		float[] results = new float[1];
		Location.distanceBetween(lat1, lon1, lat2, lon2, results);
		return results[0];
	}

	private void setDistance() {
		TextView textView = (TextView) findViewById(R.id.distance);
		double distance = getDistance(Double.parseDouble(jsonObject.optString("latitude")), Double.parseDouble(jsonObject.optString("longitude")),
				Double.parseDouble(chatInfoClass.getLocation().optString("latitude")), Double.parseDouble(chatInfoClass.getLocation().optString("longitude")));
		textView.setText(String.format("%.1f km", distance / 1000));
	}

	private void startRestaurantDetailActivity() {
		Intent intent = new Intent(getContext(), RestaurantDetailActivity.class);
		intent.putExtra("id", jsonObject.optString("id"));
		intent.putExtra("chatInfoClass", chatInfoClass.toJsonObject().toString());
		getContext().startActivity(intent);
	}

	private void setAddShareClass(JSONObject object, boolean isSelect) {
		addShareClass.setJsonObject(object, isSelect);
	}

	private void setName(JSONObject object) {
		nameTextView.setText(object.optString("name"));
	}

	private void setDeal(JSONObject object) {
		String dealString = object.optString("deal");
		if (dealString.length() > 12) {
			dealTextView.setText(dealString.substring(0, 12) + "...");
		} else {
			dealTextView.setText(dealString);
		}

	}

	private void setCategories(JSONObject object) {
		StringBuilder stringBuilder = new StringBuilder();
		JSONArray array = object.optJSONArray("categories");
		for (int i = 0; i < array.length(); i++) {
			try {
				stringBuilder.append(array.getString(i));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		categoriesTextView.setText(stringBuilder.toString());
	}

	private void setImage(JSONObject object) {
		ImageView imageView = (ImageView) findViewById(R.id.image);
		ImageDisplay.cancelDisplayImage(imageView);
		ImageDisplay.displayMyTagImage(object.optString("image"), imageView);
	}

}

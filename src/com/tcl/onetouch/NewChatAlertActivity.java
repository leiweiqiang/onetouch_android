package com.tcl.onetouch;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Window;

import com.tcl.onetouch.service.IMService;
import com.tcl.onetouch.utils.ChatInfoClass;

public class NewChatAlertActivity extends Activity {

	private static final String IS_SHOW_ALERT_FLAG = "IS_SHOW_ALERT_FLAG";
	// private final String ACTION_XMPP_LOGIN_SUCCESS =
	// "ACTION_XMPP_LOGIN_SUCCESS";
	// private final String ACTION_XMPP_LOGIN_FAILED =
	// "ACTION_XMPP_LOGIN_FAILED";

	private IMService imService = null;
	// private ProgressDialog pd;

	private ServiceConnection serviceConnection = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_new_chat_alert);
		binderService();
		// registerBoradcastReceiver();
	}

	protected void redirectToLoginActivity() {
		Intent intent = new Intent(NewChatAlertActivity.this, LoginActivity.class);
		intent.putExtra("message", getIntent().getStringExtra("message"));
		intent.putExtra(IS_SHOW_ALERT_FLAG, true);
		startActivity(intent);
		finish();
	}

	private void binderService() {
		Intent intent = new Intent(this, IMService.class);
		serviceConnection = new ServiceConnection() {
			@Override
			public void onServiceConnected(ComponentName componentName, IBinder binder) {
				imService = ((IMService.LocalBinder) binder).getService();
				// if (isIntentFromNotification()) {
				if (imService.isConnected()) {
					showNotificationConfirm();
				} else {
					redirectToLoginActivity();
					// if (pd == null) {
					// pd = ProgressDialog.show(NewChatAlertActivity.this,
					// "���ڵ�½", "���Ժ�");
					// String username =
					// UserData.getInstance(NewChatAlertActivity.this).getStringData(USERDATA_FIELD.USERNAME);
					// String password =
					// UserData.getInstance(NewChatAlertActivity.this).getStringData(USERDATA_FIELD.PASSWORD);
					// imService.connect2xmpp(username, password);
					// }
				}
				// }
			}

			@Override
			public void onServiceDisconnected(ComponentName componentName) {
				imService = null;
			}
		};
		bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}

	// private boolean isIntentFromNotification() {
	// return getIntent().getBooleanExtra(IS_SHOW_ALERT_FLAG, false);
	// }

	private void showNotificationConfirm() {
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(getIntent().getStringExtra("message"));
			showDialog(this, jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showDialog(Context context, final JSONObject jsonObject) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		String fromUser = jsonObject.optString("from");
		builder.setTitle(fromUser + "��������뷹��");
		builder.setNeutralButton("�ܾ�", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
			}
		});
		builder.setPositiveButton("����", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				imService.saveMessage(jsonObject);
				Intent intent = new Intent(NewChatAlertActivity.this, IMActivity.class);
				ChatInfoClass chatInfoClass = new ChatInfoClass(jsonObject.optString("body"));
				chatInfoClass.setNotification(true);
				intent.putExtra("chatInfoClass", chatInfoClass.toJsonObject().toString());
				startActivity(intent);
				finish();
			}
		});
		builder.show();
	}
}

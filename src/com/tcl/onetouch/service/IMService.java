package com.tcl.onetouch.service;

import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.OfflineMessageManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.tcl.onetouch.api.ChatPOIModel;
import com.tcl.onetouch.api.RideoJsonArrayRequest.RideoJsonArrayRequestListener;
import com.tcl.onetouch.provider.TABLE_KEYWORD;
import com.tcl.onetouch.provider.TABLE_MESSAGE;
import com.tcl.onetouch.utils.ChatInfoClass;
import com.tcl.onetouch.utils.Constants;
import com.tcl.onetouch.utils.UserData;
import com.tcl.onetouch.utils.UserData.USERDATA_FIELD;

public class IMService extends Service implements ConnectionListener {

	private final String TAG = "IMService";

	final String XmppHost = "52.7.122.193";
	final int XmppPort = 5222;
	public static String BASE_XMPP_SERVER_NAME = "@aliyun-05808812";

	private XMPPConnection connection;
	private static ConnectionConfiguration connectionConfig;

	private Timer exitTimer;

	private final String ACTION_NEW_RESTAURANT_CHAT = "NEW_RESTAURANT_CHAT";
	private final String ACTION_XMPP_LOGIN_SUCCESS = "ACTION_XMPP_LOGIN_SUCCESS";
	private final String ACTION_XMPP_LOGIN_FAILED = "ACTION_XMPP_LOGIN_FAILED";
	private final String ACTION_XMPP_USER_CONFLIT = "ACTION_XMPP_USER_CONFLIT";

	private static final String IS_SHOW_ALERT_FLAG = "IS_SHOW_ALERT_FLAG";

	private int HashCode = 0;

	private IBinder binder = new IMService.LocalBinder();

	static {
		try {
			Class.forName("org.jivesoftware.smack.ReconnectionManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCreate() {
		Connection.DEBUG_ENABLED = false;
		// ProviderManager pm = ProviderManager.getInstance();
		// configure(pm);

		connectionConfig = new ConnectionConfiguration(XmppHost, XmppPort);
		connectionConfig.setSASLAuthenticationEnabled(false);
		connectionConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
		connectionConfig.setReconnectionAllowed(true);
		connectionConfig.setSendPresence(false);
		// Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.manual);
		connection = new XMPPConnection(connectionConfig);
		connection.addConnectionListener(this);
		super.onCreate();
	}

	// @Override
	// public void onDestroy() {
	// connection.disconnect();
	// super.onDestroy();
	// }

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
	}

	public class LocalBinder extends Binder {
		public IMService getService() {
			return IMService.this;
		}
	}

	class timetask extends TimerTask {
		@Override
		public void run() {
			String username = UserData.getInstance(IMService.this).getStringData(USERDATA_FIELD.USERNAME);
			String password = UserData.getInstance(IMService.this).getStringData(USERDATA_FIELD.PASSWORD);
			if (username != null && password != null) {
				try {
					if (connection.isConnected() == false) {
						connection.connect();
					}
					if (connection.isAuthenticated() == false) {
						connection.login(username, password, "android");
					}
				} catch (Exception e) {
					exitTimer.schedule(new timetask(), 1000);
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	private void recieveMessage(XMPPConnection con) {
		con.getChatManager().addChatListener(new ChatManagerListener() {

			@Override
			public void chatCreated(Chat chat, boolean arg1) {
				chat.addMessageListener(new MessageListener() {
					@Override
					public void processMessage(Chat chat, org.jivesoftware.smack.packet.Message message) {
						onNewMessageRecv(message);
						// handler.sendMessage(handler.obtainMessage(MESSAGE_NEW_MESSAGE_RECEIVED,
						// message));
					}
				});

			}
		});
	}

	private void onNewMessageRecv(org.jivesoftware.smack.packet.Message message) {
		if (HashCode != message.hashCode()) {
			HashCode = message.hashCode();
			try {
				JSONObject msgJsonObject = new JSONObject();
				msgJsonObject.put("from", getName(message.getFrom()));
				msgJsonObject.put("to", getName(message.getTo()));
				msgJsonObject.put("time", "" + new Date().getTime());
				msgJsonObject.put("body", message.getBody());
				msgJsonObject.put("subject", TextUtils.isEmpty(message.getSubject()) ? "" : message.getSubject());
				msgJsonObject.put("isIncomming", "true");

				if (message.getBody() != null) {
					String subject = message.getSubject();
					if (!TextUtils.isEmpty(subject) && subject.equalsIgnoreCase(Constants.IM_MESSAGE_DATING_REST)) {
						onNewRestraurantChatMessage(msgJsonObject);
					} else {
						saveMessage(msgJsonObject);
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private void onNewRestraurantChatMessage(JSONObject msgJsonObject) {
		Intent mIntent = new Intent(ACTION_NEW_RESTAURANT_CHAT);
		mIntent.putExtra("msgJsonObject", msgJsonObject.toString());
		sendBroadcast(mIntent);
	}

	private String getName(String name) {
		int pos = name.indexOf("@");
		String ret = name.substring(0, pos);
		return ret;
	}

	public void saveMessage(JSONObject msgJsonObject) {
		ContentValues values = new ContentValues();
		values.put(TABLE_MESSAGE.USER.name(), UserData.getInstance(this).getStringData(USERDATA_FIELD.USERNAME));
		values.put(TABLE_MESSAGE.MSG_FROM.name(), msgJsonObject.optString("from"));
		values.put(TABLE_MESSAGE.MSG_TO.name(), msgJsonObject.optString("to"));
		values.put(TABLE_MESSAGE.DATE.name(), "" + new Date().getTime());
		values.put(TABLE_MESSAGE.MSG_CONTENT.name(), msgJsonObject.optString("body"));
		values.put(TABLE_MESSAGE.SUBJECT.name(), msgJsonObject.optString("subject"));
		values.put(TABLE_MESSAGE.IS_INCOMMING_MSG.name(), msgJsonObject.optString("isIncomming"));
		getContentResolver().insert(TABLE_MESSAGE.CONTENT_URI, values);
		getChatPOI(msgJsonObject.optString("body"));
	}

	private void getChatPOI(String text) {
		ChatPOIModel model = new ChatPOIModel(text, new RideoJsonArrayRequestListener() {

			@Override
			public void onDataRecv(JSONArray jsonArray) {
				saveChatPOI(jsonArray);
			}

			@Override
			public void onDataError(VolleyError error) {

			}
		});

		model.fetch();
	}

	private void saveChatPOI(JSONArray jsonArray) {
		if (jsonArray.length() > 0) {
			for (int i = 0; i < jsonArray.length(); i++) {
				String keyword = jsonArray.optString(i);
				Cursor cursor = getContentResolver().query(TABLE_KEYWORD.CONTENT_URI, null, TABLE_KEYWORD.KEYWORD + "=?", new String[] { keyword }, null);
				if (cursor.getCount() == 0) {
					ContentValues values = new ContentValues();
					values.put(TABLE_KEYWORD.KEYWORD.name(), keyword);
					Uri uri = getContentResolver().insert(TABLE_KEYWORD.CONTENT_URI, values);
				}
				cursor.close();
			}
		}
	}

	private void startGetOfflineMessage() {

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					getOfflineMsg();
				} catch (XMPPException e1) {
					e1.printStackTrace();
				}
			}
		}).start();
	}

	private void getOfflineMsg() throws XMPPException {
		OfflineMessageManager offlineManager = new OfflineMessageManager(connection);
		Iterator<org.jivesoftware.smack.packet.Message> it = offlineManager.getMessages();
		while (it.hasNext()) {
			org.jivesoftware.smack.packet.Message message = it.next();
			onNewMessageRecv(message);
		}
		offlineManager.deleteMessages();
		connection.sendPacket(new Presence(Presence.Type.available));
	}

	private void onLoginSuccess(String username, String password) {
		UserData.getInstance(this).setStringData(USERDATA_FIELD.USERNAME, username);
		UserData.getInstance(this).setStringData(USERDATA_FIELD.PASSWORD, password);
		recieveMessage(connection);
		startGetOfflineMessage();
	}

	public void connect2xmpp(final String username, final String password) {

		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					Log.v(TAG, "start connection");

					connection.connect();
					if (connection.isConnected()) {
						connection.login(username, password, "android");
					}
					// connection.addConnectionListener(new ConnectionListener()
					// {
					//
					// @Override
					// public void reconnectionSuccessful() {
					// Log.v(TAG, "reconnectionSuccessful");
					// }
					//
					// @Override
					// public void reconnectionFailed(Exception arg0) {
					// Log.v(TAG, "reconnectionFailed");
					//
					// }
					//
					// @Override
					// public void reconnectingIn(int arg0) {
					// Log.v(TAG, "reconnectingIn");
					//
					// }
					//
					// @Override
					// public void connectionClosedOnError(Exception arg0) {
					// Log.v(TAG, "connectionClosedOnError");
					// exitTimer = new Timer();
					// exitTimer.schedule(new timetask(), 1000);
					// }
					//
					// @Override
					// public void connectionClosed() {
					// Log.v(TAG, "connectionClosed");
					//
					// }
					// });
					onLoginSuccess(username, password);
					sendBroadcast(new Intent(ACTION_XMPP_LOGIN_SUCCESS));
				} catch (Exception xee) {
					if (xee instanceof XMPPException) {
						XMPPException xe = (XMPPException) xee;
						final XMPPError error = xe.getXMPPError();
						int errorCode = 0;
						if (error != null) {
							errorCode = error.getCode();
						}
					}

					sendBroadcast(new Intent(ACTION_XMPP_LOGIN_FAILED));
				}
				Looper.loop();
			}
		}).start();
	}

	public void sendRestaurantMsg(ChatInfoClass chatInfoClass) {
		if (connection.isConnected() == false) {
			return;
		}
		String sendTo = chatInfoClass.getSendTo().getName();
		String sendFrom = chatInfoClass.getSendFrom().getName();
		Chat chat = connection.getChatManager().createChat(sendTo + "@" + connection.getServiceName(), null);
		Message message = new Message();
		message.setTo(sendTo + "@" + connection.getServiceName());
		message.setFrom(sendFrom + "@" + connection.getServiceName());
		message.setSubject(Constants.IM_MESSAGE_DATING_REST);
		message.setBody(chatInfoClass.toJsonObject().toString());
		try {
			chat.sendMessage(message);
			saveMessage(chatInfoClass, message.getSubject());
		} catch (XMPPException e) {
			e.printStackTrace();
		}
	}

	private void saveMessage(ChatInfoClass chatInfo, String subject) {
		ContentValues values = new ContentValues();
		values.put(TABLE_MESSAGE.USER.name(), UserData.getInstance(this).getStringData(USERDATA_FIELD.USERNAME));
		values.put(TABLE_MESSAGE.MSG_FROM.name(), chatInfo.getSendFrom().getName());
		values.put(TABLE_MESSAGE.MSG_TO.name(), chatInfo.getSendTo().getName());
		values.put(TABLE_MESSAGE.DATE.name(), "" + new Date().getTime());
		values.put(TABLE_MESSAGE.MSG_CONTENT.name(), chatInfo.toJsonObject().toString());
		values.put(TABLE_MESSAGE.SUBJECT.name(), subject);
		values.put(TABLE_MESSAGE.IS_INCOMMING_MSG.name(), "false");
		Uri uri = getContentResolver().insert(TABLE_MESSAGE.CONTENT_URI, values);
	}

	public interface SendImMessageListener {
		public void onSendImMessageSuccess();

		public void onSendImMessageFailed();
	}

	public void sendImMessage(final ChatInfoClass chatInfoClass, final String msgString, final SendImMessageListener listener) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					if (connection.isConnected() == false) {
						// con.connect();
						listener.onSendImMessageFailed();
						return;
					}

					if (connection.isConnected()) {
						String to = chatInfoClass.getSendTo().getName();
						String from = chatInfoClass.getSendFrom().getName();

						Chat chat = connection.getChatManager().createChat(to + "@" + connection.getServiceName(), null);
						chat.sendMessage(msgString);
						listener.onSendImMessageSuccess();
						saveMessage(chatInfoClass.getSendFrom().getName(), chatInfoClass.getSendTo().getName(), msgString, "");
					} else {
						listener.onSendImMessageFailed();
					}
				} catch (Exception e) {
					listener.onSendImMessageFailed();
					e.printStackTrace();
				}
			}
		}).start();

	}

	private void saveMessage(String from, String to, String msg, String subject) {
		ContentValues values = new ContentValues();
		values.put(TABLE_MESSAGE.USER.name(), UserData.getInstance(this).getStringData(USERDATA_FIELD.USERNAME));
		values.put(TABLE_MESSAGE.MSG_FROM.name(), from);
		values.put(TABLE_MESSAGE.MSG_TO.name(), to);
		values.put(TABLE_MESSAGE.DATE.name(), "" + new Date().getTime());
		values.put(TABLE_MESSAGE.MSG_CONTENT.name(), msg);
		values.put(TABLE_MESSAGE.SUBJECT.name(), subject);
		values.put(TABLE_MESSAGE.IS_INCOMMING_MSG.name(), "false");
		Uri uri = getContentResolver().insert(TABLE_MESSAGE.CONTENT_URI, values);
		getChatPOI(msg);

	}

	@Override
	public void connectionClosed() {
		Log.v(TAG, "connectionClosed--->");
		new ToastMessageTask().execute("连接已断开");
	}

	private class ToastMessageTask extends AsyncTask<String, String, String> {
		String toastMessage;

		@Override
		protected String doInBackground(String... params) {
			toastMessage = params[0];
			return toastMessage;
		}

		protected void OnProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}

		// This is executed in the context of the main GUI thread
		protected void onPostExecute(String result) {
			Toast toast = Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	@Override
	public void connectionClosedOnError(Exception e) {
		Log.v(TAG, "connectionClosedOnError--->");
		if (e.getMessage().contains("conflict")) { // 被挤掉线
			new ToastMessageTask().execute("用户已经在其它地点登陆");
			sendBroadcast(new Intent(ACTION_XMPP_USER_CONFLIT));
		}
	}

	@Override
	public void reconnectingIn(int arg0) {
		Log.v(TAG, "reconnectingIn--->");
		new ToastMessageTask().execute("正在重新连接服务器");
	}

	@Override
	public void reconnectionFailed(Exception arg0) {
		Log.v(TAG, "reconnectionFailed--->");
		new ToastMessageTask().execute("重新连接服务器失败");
	}

	@Override
	public void reconnectionSuccessful() {
		Log.v(TAG, "reconnectionSuccessful--->");
		new ToastMessageTask().execute("重新连接服务器成功");
	}

	public boolean isConnected() {
		return connection.isConnected();
	}
}

package com.tcl.onetouch.provider;

import android.net.Uri;

public enum TABLE_KEYWORD {
	_id, //
	KEYWORD;

	public static String getCreateTableSqlString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("CREATE TABLE IF NOT EXISTS ");
		stringBuilder.append(DB_TABLE.KEYWORDS.name());
		stringBuilder.append("(");
		for (TABLE_KEYWORD fieldsName : TABLE_KEYWORD.values()) {
			stringBuilder.append(fieldsName.toString());
			if (fieldsName != TABLE_KEYWORD.values()[TABLE_KEYWORD.values().length - 1]) {
				if (fieldsName.toString().equals("_id")) {
					stringBuilder.append(" INTEGER PRIMARY KEY");
				}
				stringBuilder.append(",");
			}
		}
		stringBuilder.append(")");
		return stringBuilder.toString();
	}

	public static final Uri CONTENT_URI = Uri.parse("content://" + DB_TABLE.AUTHORITY + "/" + DB_TABLE.KEYWORDS.name());

}

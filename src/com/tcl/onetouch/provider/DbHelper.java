package com.tcl.onetouch.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "data.db";

	public DbHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
	}

	public DbHelper(Context context, String name, int version) {
		this(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public DbHelper(Context context) {
		this(context, DATABASE_NAME, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_KEYWORD.getCreateTableSqlString());
		db.execSQL(TABLE_MESSAGE.getCreateTableSqlString());
		db.execSQL(TABLE_USER.getCreateTableSqlString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}
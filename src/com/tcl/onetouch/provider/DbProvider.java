package com.tcl.onetouch.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

public class DbProvider extends ContentProvider {
	public static final int DATABASE_VERSION = 1;
	public static final String DATA_DB_NAME = "data.db";

	public static final String AUTHORITY = "com.tcl.onetouch.provider.DbProvider";
	public static final Uri CONTENT_KEYWORD_URI = Uri.parse("content://" + AUTHORITY + "/" + DB_TABLE.KEYWORDS.name());

	protected final int SEG_TABLE_NAME = 0;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		DB_TABLE tableName = DB_TABLE.valueOf(uri.getPathSegments().get(SEG_TABLE_NAME));
		int ret = DbManager.getInstance(getContext()).delete(tableName, selection, selectionArgs);
		if (ret > 0) {
			DbManager.getInstance(getContext()).execSQL("VACUUM " + tableName);
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return ret;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		DB_TABLE tableName = DB_TABLE.valueOf(uri.getPathSegments().get(SEG_TABLE_NAME));
		long rowId = DbManager.getInstance(getContext()).insert(tableName, values);
		if (rowId > 0) {
			Uri noteUri = Uri.parse(uri.toString() + "/" + rowId);
			getContext().getContentResolver().notifyChange(noteUri, null);
			return noteUri;
		}
		return uri;
	}

	@Override
	public boolean onCreate() {
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		DB_TABLE tableName = DB_TABLE.valueOf(uri.getPathSegments().get(SEG_TABLE_NAME));
		Cursor cursor = null;
		cursor = DbManager.getInstance(getContext()).query(tableName, projection, selection, selectionArgs, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}

}

package com.tcl.onetouch.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbManager {
	private volatile DbHelper dbHelper;
	private Context mContext;
	private volatile static DbManager dbManager;

	private DbManager(Context context) {
		this.mContext = context;
		dbHelper = new DbHelper(context);
	}

	public static synchronized DbManager getInstance(Context context) {
		if (dbManager == null) {
			synchronized (DbManager.class) {
				if (dbManager == null) {
					dbManager = new DbManager(context);
				}
			}
		}
		return dbManager;
	}

	public DbHelper getDbHelper() {
		return dbHelper;
	}

	public synchronized int insert(DB_TABLE table, ContentValues values) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		int insertID = (int) db.insert(table.name(), null, values);
		return insertID;
	}

	@Override
	protected void finalize() throws Throwable {
		if (dbHelper != null) {
			dbHelper.close();
		}
		super.finalize();
	}

	public synchronized void execSQL(String sqlString) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		db.execSQL(sqlString);
	}

	public synchronized int delete(DB_TABLE table, String wheres, String[] whereArgs) {
		int ret = 0;
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		if (wheres == null && whereArgs == null) {
			ret = db.delete(table.name(), null, null);
		} else {
			ret = db.delete(table.name(), wheres + " = ?", whereArgs);
		}
		return ret;
	}

	public synchronized Cursor query(DB_TABLE table, String[] columns, String selections, String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String _sortOrderString = null;
		String _groupString = null;
		String[] src1 = null;
		if (sortOrder != null) {
			src1 = sortOrder.split(";");
			for (String s1 : src1) {
				String[] src2 = s1.split(":");
				if (src2.length == 1) {
					_sortOrderString = src2[0];
				} else {
					if (src2[0].indexOf("group") == 0) {
						_groupString = src2[1];
					} else if (src2[0].indexOf("limit") == 0) {

					}
				}
			}
		}
		Cursor cursor = db.query(table.name(), columns, selections, selectionArgs, _groupString, null, _sortOrderString);
		return cursor;
	}

	public synchronized Cursor rawQuery(String selectClause, String[] selectArgs) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectClause, selectArgs);
		return cursor;
	}

	public synchronized void update(DB_TABLE table, ContentValues values, String wheres, String[] whereArgs) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		db.update(table.name(), values, wheres, whereArgs);
	}

}
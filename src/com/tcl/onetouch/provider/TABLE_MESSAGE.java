package com.tcl.onetouch.provider;

import android.net.Uri;

public enum TABLE_MESSAGE {
	_id, //
	USER,
	MSG_FROM, //
	MSG_TO, //
	DATE, //
	SUBJECT, //
	MSG_CONTENT, //
	IS_INCOMMING_MSG;

	public static String getCreateTableSqlString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("CREATE TABLE IF NOT EXISTS ");
		stringBuilder.append(DB_TABLE.MESSAGE.name());
		stringBuilder.append("(");
		for (TABLE_MESSAGE fieldsName : TABLE_MESSAGE.values()) {
			stringBuilder.append(fieldsName.toString());
			if (fieldsName != TABLE_MESSAGE.values()[TABLE_MESSAGE.values().length - 1]) {
				if (fieldsName.toString().equals("_id")) {
					stringBuilder.append(" INTEGER PRIMARY KEY");
				}
				stringBuilder.append(",");
			}
		}
		stringBuilder.append(")");
		return stringBuilder.toString();
	}

	public static final Uri CONTENT_URI = Uri.parse("content://" + DB_TABLE.AUTHORITY + "/" + DB_TABLE.MESSAGE.name());

}

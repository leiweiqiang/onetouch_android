package com.tcl.onetouch.provider;

import android.net.Uri;

public enum TABLE_USER {
	_id, //
	USERNAME, //
	NICKNAME,//
	IMAGE;

	public static String getCreateTableSqlString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("CREATE TABLE IF NOT EXISTS ");
		stringBuilder.append(DB_TABLE.USER.name());
		stringBuilder.append("(");
		for (TABLE_USER fieldsName : TABLE_USER.values()) {
			stringBuilder.append(fieldsName.toString());
			if (fieldsName != TABLE_USER.values()[TABLE_USER.values().length - 1]) {
				if (fieldsName.toString().equals("_id")) {
					stringBuilder.append(" INTEGER PRIMARY KEY");
				}
				stringBuilder.append(",");
			}
		}
		stringBuilder.append(")");
		return stringBuilder.toString();
	}

	public static final Uri CONTENT_URI = Uri.parse("content://" + DB_TABLE.AUTHORITY + "/" + DB_TABLE.USER.name());

}
